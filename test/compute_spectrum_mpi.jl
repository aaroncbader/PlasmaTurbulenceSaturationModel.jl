using LinearAlgebra, MPI, StellaratorOptimization, VMEC, GeneTools, PlasmaTurbulenceSaturationModel, JLD2
#ENV["JULIA_DEBUG"] = PlasmaTurbulenceSaturationModel

BLAS.set_num_threads(4) # Change this to whatever works for you architecture
MPI.Init()
rank = MPI.Comm_rank(MPI.COMM_WORLD)
nml = VMEC.read_vmec_namelist("/home/bfaber/.julia/dev/StellaratorOptimization/test/input.aten")
vmec = run_vmec(MPI.COMM_WORLD, nml)
surface = VmecSurface(0.5, vmec)
model = PTSM.setup_linear_model(surface, Δkx = 0.20, Δky = 0.20, nkx = 6, nky = 5, soln_interval = 12π, n_ev = 1, prec = Float32, n_points = 768, solver = PTSM.ArnoldiMethod())
PTSM.compute_linear_spectrum!(model, MPI.COMM_WORLD)

if rank == 0
    save_object("test_model_small.jld2", model)
end