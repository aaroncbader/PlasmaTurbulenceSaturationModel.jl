using LinearAlgebra, MPI, StellaratorOptimization, VMEC, GeneTools, PlasmaTurbulenceSaturationModel, JLD2
#ENV["JULIA_DEBUG"] = PlasmaTurbulenceSaturationModel

BLAS.set_num_threads(4) # Change this to whatever works for you architecture
MPI.Init()
rank = MPI.Comm_rank(MPI.COMM_WORLD)
model = load_object("test_model_small.jld2")
couplings, max_couplings = PTSM.non_zonal_coupling(model, MPI.COMM_WORLD)

if rank == 0
    save_object("non_zonal_couplings.jld2", couplings)
    save_object("max_non_zonal_couplings.jld2", max_couplings)
end