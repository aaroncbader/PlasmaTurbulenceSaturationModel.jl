module PlasmaTurbulenceSaturationModel
using Requires
using Statistics
using StaticArrays
using SparseArrays
using StructArrays
using LabelledArrays
using LinearAlgebra
using FFTViews
using FiniteDifferences
using FiniteDiff
using SparseDiffTools
using SparsityDetection
using Interpolations
using Polynomials
using JacobiDavidson
using KrylovKit
using FastGaussQuadrature
using Polyester
using JLD2

using PlasmaEquilibriumToolkit

const PTSM = PlasmaTurbulenceSaturationModel
export PTSM

include("types.jl")
include("plasma.jl")
include("linear_problem.jl")
include("spectral_grid.jl")
include("drift_wave.jl")
include("geometry.jl")
include("fluid_model.jl")
include("utils.jl")
include("linear_solvers.jl")
include("compute_spectrum.jl")
include("coupling.jl")
include("input_output.jl")
include("itg_three_field/itg_three_field.jl")

function __init__()
  # Load the necessary interfaces
  @require VMEC="2b46c670-0004-47b5-bf0a-1741584931e9" begin 
    @require GeneTools="34794f4d-a057-4455-8bb4-5e6606ed443e" include("interfaces/vmec_interface.jl")
  end

  @require NE3DLE="6356e481-0c20-445f-8811-d253bdede59b" begin
    include("interfaces/ne3dle_interface.jl")
  end

  @require StellaratorOptimization="f2f44f23-99b2-4d7c-931e-31858dc08d61" begin
    include("optimization/stellopt_interface.jl")
    include("optimization/optimization_targets.jl")
  end

  @require StellaratorOptimization="f2f44f23-99b2-4d7c-931e-31858dc08d61" begin
    @require MPI="da04e1cc-30fd-572f-bb4f-1f8673147195" include("optimization/stellopt_interface_mpi.jl")
  end

  @require CairoMakie="13f3f980-e62b-5c42-98c6-ff1f3baf88f0" begin
    using .CairoMakie
    using LaTeXStrings
    using Printf
    include("visualization/growth_contour.jl")
    include("visualization/mode_spectrum.jl")
    include("visualization/mode_structure.jl")
    include("visualization/tau_contour.jl")
    include("visualization/marg_contour.jl")
  end

  @require GLMakie="e9467ef8-e4e7-5192-8a1a-b1aee30e663a" begin
    using .GLMakie
    using LaTeXStrings
    using Printf
    include("visualization/growth_contour.jl")
    include("visualization/mode_spectrum.jl")
    include("visualization/mode_structure.jl")
    include("visualization/tau_contour.jl")
    include("visualization/marg_contour.jl")
  end
end

end
