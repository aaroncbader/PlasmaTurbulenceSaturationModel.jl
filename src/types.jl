"""
    abstract type AbstractFluid

The abstract type to represent a fluid model
"""
abstract type AbstractFluidModel end

"""
    abstract type AbstractGeometryParameters

The abstract type to represent the geometry parameters that are input
for a fluid model calcluation
"""
abstract type AbstractGeometryParameters end

"""
    abstract type AbstractFieldlineGeometry

The abstract type to represent the geometry coefficients along a
fieldline for use in a fluid model calculation
"""
abstract type AbstractFieldlineGeometry end

"""
    abstract type SolverMethod

      Abstract type to repesent an iterative method algorithm

      # Available Algorithms
      # - `JacobiDavidson()` : Complex Jacobi-Davidson method
      # - `ArnoldiMethod()` : Complex implicitly restarted ArnoldiMethod method from KrylovKit.jl
"""
abstract type AbstractSolverMethod end

"""
    NullParameters

Singleton struct representing no geometry parameters
"""
struct NullParameters <: AbstractGeometryParameters end


const SplineType = ScaledInterpolation
const BoundsTuple{T} = NamedTuple{(:min, :max), Tuple{T, T}} where {T}

