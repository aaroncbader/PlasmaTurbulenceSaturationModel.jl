"""
    FieldlineGeometry <: AbstractFieldlineGeometry

Composite type to hold geometry elements at a single point along a magnetic field line
using GENE geometry normalizations

# Fields
- `z_span::NamedTuple{(:min, :max)}` : not normalized to π
- `g::NamedTuple{(:xx, :xy, :yy)}` : Static vector holding the 6 independent metric elements
- `B` : Magnetic field normalized to magnetic field at LCFS
- `jac` : Scalar Jacobian of the magnetic coordinate transformation
- `Bx∇B::NamedTuple{(:∇x, :∇y)}` : (∇x = B × ∇B ⋅ ∇X / B², ∇y = B × ∇B ⋅ ∇Y / B²)
- `∂B∂z` : ∂B/∂Z, Z = θ in GENE normalizations
"""
struct FieldlineGeometry{T} <: AbstractFieldlineGeometry
    z_span::NamedTuple
    g::NamedTuple
    B::SplineType
    jac::SplineType
    Bx∇B::NamedTuple
    ∂B∂z::SplineType
    parameters::SLArray{Tuple{3}, T, 1, 3, (:ι, :s_hat, :nfp)}
end


function FieldlineGeometry(g::AbstractVector{SVector{6, T}},
                           B::AbstractVector{T},
                           J::AbstractVector{T},
                           K1::AbstractVector{T},
                           K2::AbstractVector{T},
                           dBdZ::AbstractVector{T},
                           θ::Union{AbstractVector{T}, AbstractRange};
                           ι::T = 1.0,
                           s_hat::T = 1.0,
                           nfp = 1.0,
                           FloatType = Float64,
                          ) where T
    gxx = map(i -> i[1], g)
    gxy = map(i -> i[2], g)
    gyy = map(i -> i[3], g)
    θ_range = vector2range(θ; convert_type = FloatType)
    itp(x) = scale(interpolate(x, BSpline(Cubic(Throw(OnGrid())))), θ_range)
    gxx_spline = itp(convert.(FloatType, gxx))
    gxy_spline = itp(convert.(FloatType, gxy))
    gyy_spline = itp(convert.(FloatType, gyy))
    B_spline = itp(convert.(FloatType, B))
    jac_spline = itp(convert.(FloatType, J))
    Bx∇B_x_spline = itp(convert.(FloatType, K1))
    Bx∇B_y_spline = itp(convert.(FloatType, K2))
    ∂B∂z_spline = itp(convert.(FloatType, dBdZ))
    g_spline = (xx = gxx_spline, xy = gxy_spline, yy = gyy_spline)
    Bx∇B_spline = (∇x = Bx∇B_x_spline, ∇y = Bx∇B_y_spline)
    θ_span = (min = first(θ_range), max = last(θ_range))
    return FieldlineGeometry{FloatType}(θ_span, g_spline, B_spline, 
                                        jac_spline, Bx∇B_spline, ∂B∂z_spline,
                                        SLVector((ι = ι, s_hat = s_hat, nfp = nfp)))
end

function polarization_damping(z::S,
                              kx::T,
                              ky::T,
                              geom::FieldlineGeometry;
                             ) where {S, T}
    return polarization_damping(z, kx, ky, 
                                geom.g.xx, geom.g.xy, geom.g.yy, geom.B)
end

function polarization_damping(z::S,
                              kx::T,
                              ky::T,
                              gxx::SplineType,
                              gxy::SplineType,
                              gyy::SplineType,
                              B::SplineType;
                             ) where {S, T}
    return (kx^2*gxx(z) + 2*kx*ky*gxy(z) + ky^2*gyy(z))/(B(z)^2)
end
  
function curvature_drive(z::S,
                         kx::T,
                         ky::T,
                         geom::FieldlineGeometry;
                        ) where{S, T}
    return curvature_drive(z, kx, ky, geom.B, geom.Bx∇B.∇x, geom.Bx∇B.∇y)
end

function curvature_drive(z::S,
                         kx::T,
                         ky::T,
                         B::SplineType,
                         Bx∇B_∇x::SplineType,
                         Bx∇B_∇y::SplineType;
                        ) where {S, T}
    # This requires a negative sign in front of K1 as the geometry
    # component given in GENE normalizations is the negative of
    # the proper defintion (I believe due to right-handed vs. left-handed)
    return 2/B(z)*(-kx*Bx∇B_∇x(z) + ky*Bx∇B_∇y(z))
end

function damping_drive_term(z::S,
                            kx::T,
                            ky::T,
                            geom::FieldlineGeometry,
                           ) where {S, T}
    return ((kx^2*geom.g.xx(z) + 2*kx*ky*geom.g.xy(z) + ky^2*geom.g.yy(z))/(geom.B(z)^2),
             2/geom.B(z)*(-kx*geom.Bx∇B.∇x(z) + ky*geom.Bx∇B.∇y(z)))
end
