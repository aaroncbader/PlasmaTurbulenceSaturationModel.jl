function growth_rates(kys::Union{T, AbstractVector{T}},
                      surface::E;
                      kwargs...
                     ) where {T, E <: AbstractGeometry}
  nky = length(kys)
  ky_range = vector2range(kys)  
  Δky = step(ky_range)
  model = setupLinearModel(surface; nky = nky, Δky = Δky, kwargs...)
  computeLinearSpectrum!(model)

  γ_values = zeros(T, nky)
  for k in 1:nky
    for j in 1:nKxModes(model)
      γ_values[k] = imag(eigenvalues(model, j, k, 1)) > γ_values[k] ? imag(eigenvalues(model, j, k, 1)) : γ_values[k]
    end
  end

  return nky > 1 ? tuple(γ_values...) : γ_values[1]
end

function non_zonal_correlation_times(eq::E;
                                     kwargs...        
                                    ) where {E <: AbstractGeometry}
  model = setupLinearModel(eq; kwargs...)

  computeLinearSpectrum!(model)

  _, _, _, _, τ_nzf, _ = threeWaveCoupling(model)

  τ_sum = sum(real(τ_nzf))
  return 1/τ_sum
end

function non_zonal_τC(eq::E;
                      kwargs...
                     ) where {E <: AbstractGeometry}
  model = setupLinearModel(eq; kwargs...)
  computeLinearSpectrum!(model)

  _, _, _, _, τ_nzf, C_nzf = threeWaveCoupling(model)

  τC_sum = sum(abs.(τ_nzf .* C_nzf))
  return 1/τC_sum
end
