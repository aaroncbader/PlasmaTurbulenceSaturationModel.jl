using .MPI

function compute_linear_spectrum!(model::FluidModel{N, T},
                                  comm::MPI.Comm;
                                 ) where {N, T}
    rank = MPI.Comm_rank(comm)
    comm_size = MPI.Comm_size(comm)
    n_kx = n_kx_modes(model.grid, neg_kx = false)
    n_ky = n_ky_modes(model.grid)
    n_points = model.problem.n_points

    if rank == 0
        rank_status = fill(true, comm_size - 1)
        indices = CartesianIndices((1, 1:n_ky))
        I, S = nothing, nothing
        for i in eachindex(rank_status)
            try
                I, S = !isnothing(S) ? iterate(indices, S) : iterate(indices)
            catch
                I, S = CartesianIndex(-1, -1), S
            end
            MPI.send(I, comm; dest = i, tag = i)
            if I == CartesianIndex(-1, -1)
                rank_status[i] = false 
            end
        end

        finished = all(r -> !r, rank_status) 
        while !finished
            println("Looking for a recv with comm ", comm, " with size ", comm_size)
            r = MPI.recv(comm; source = MPI.ANY_SOURCE, tag = MPI.ANY_TAG)
            J = MPI.recv(comm; source = r, tag = 2 * comm_size + r)

@debug "Head received $J from rank $r"
        
            for k in 1:n_kx
                K = CartesianIndex(k, J[2])

@debug "Head is assigning data to $K"
                ωr = MPI.recv(comm; source = r, tag = 3 * comm_size + r)
                βr = MPI.recv(comm; source = r, tag = 4 * comm_size + r)
                update_spectrum!(model, K, ωr, βr)

@debug model.spectrum[K][1][1]

            end

@debug "Head received kx spectrum data from rank $r"

            try
                I, S = !isnothing(S) ? iterate(indices, S) : iterate(indices)
            catch
                I, S = CartesianIndex(-1, -1), S
            end

@debug "Head sending $I to rank $r with tag $r"

            MPI.send(I, comm; dest = r, tag = r)
            if I == CartesianIndex(-1, -1)
                rank_status[r] = false 
            end
            if all(i -> !i, rank_status)
                finished = true
            end
        end
    else
        I = CartesianIndex(0, 0)
        finished = false
    
        ωs = Vector{Complex{T}}(undef, n_ev(model.spectrum[1]))
        βs = Matrix{Complex{T}}(undef, N * n_points, n_ev(model.spectrum[1]))
        stat = MPI.Status()
        while !finished
            I, status = MPI.recv(comm, stat; source = 0, tag = rank)

@debug "Rank $rank received $I from head with tag $rank"

            if I != CartesianIndex(-1, -1)
                for j in 1:n_kx
                    J = CartesianIndex(j, I[2])
                    init = J[1] == 1 ? true : false

@debug "Rank $rank is computing mode $J with init = $init"

                    compute_linear_mode!(model, J, init = init, scan_dir = :down)

@debug "Rank $rank has computed mode $J with γ $(model.spectrum[J][1][1])"

                end

@debug "Rank $rank finished, sending $rank to head with tag $(comm_size + rank)"

                MPI.send(rank, comm; dest = 0, tag = comm_size + rank)

@debug "Rank $rank sending $I to head with tag $(2 * comm_size + rank)"

                MPI.send(I, comm; dest = 0, tag = 2 * comm_size + rank)

                for j in 1:n_kx 
                    J = CartesianIndex(j, I[2])
                    for m in eachindex(ωs)
                        ωs[m] = model.spectrum[J][m][1]
                        for n in 1:N
                            βs[(n-1) *n_points + 1 : n *n_points, m] .= model.spectrum[J][m].β[n].itp.coefs.parent[2:end-1]
                        end
                    end
                    MPI.send(ωs, comm; dest = 0, tag = 3 * comm_size + rank)
                    MPI.send(βs, comm; dest = 0, tag = 4 * comm_size + rank)
                end
            else
                finished = true
            end
        end
    end
end

function zonal_coupling(model::FluidModel{N, T},
                        comm::MPI.Comm;
                       ) where {N, T}
    rank = MPI.Comm_rank(comm)
    comm_size = MPI.Comm_size(comm)
    n_kx = n_kx_modes(model.grid, neg_kx = false)
    n_ky = n_ky_modes(model.grid)

    if rank == 0
        max_couplings = Matrix{CouplingData{T}}(undef, n_kx, n_ky)
        summed_couplings = Matrix{T}(undef, n_kx, n_ky)
        rank_status = fill(true, comm_size - 1)
        indices = CartesianIndices((max_couplings))
        I, S = nothing, nothing
        for i in eachindex(rank_status)
            try
                I, S = !isnothing(S) ? iterate(indices, S) : iterate(indices)
            catch
                I, S = CartesianIndex(-1, -1), S
            end
            MPI.send(I, comm; dest = i, tag = i)
            if I == CartesianIndex(-1, -1)
                rank_status[i] = false 
            end
        end

        finished = all(r -> !r, rank_status) 
        while !finished
            r = MPI.recv(comm; source = MPI.ANY_SOURCE, tag = MPI.ANY_TAG)
            J = MPI.recv(comm; source = r, tag = 2 * comm_size + r)

@debug "Head received $J from rank $r"

            c_data  = MPI.recv(comm; source = r, tag = 3 * comm_size + r)

@debug "Head received coupling data from rank $r"

            summed_couplings[J] = c_data[1]
            max_couplings[J] = c_data[2]
            try
                I, S = !isnothing(S) ? iterate(indices, S) : iterate(indices)
            catch
                I, S = CartesianIndex(-1, -1), S
            end

@debug "Head sending $I to rank $r with tag $r"

            MPI.send(I, comm; dest = r, tag = r)
            if I == CartesianIndex(-1, -1)
                rank_status[r] = false 
            end
            if all(i -> !i, rank_status)
                finished = true
            end
        end
        return summed_couplings, StructArray(max_couplings)
    else
        I = CartesianIndex(0, 0)
        coupling_data = Vector{Tuple{T, CouplingData{T}}}(undef, n_kx_modes(model.grid, neg_kx = false))
        while I != CartesianIndex(-1, -1)
            I = MPI.recv(comm; source = 0, tag = rank)

@debug "Rank $rank received $I from head with tag $rank"

            if I != CartesianIndex(-1, -1)
                for j in 1:n_kx
                    J = CartesianIndex(j, I[2])
                    K = triplet_matching(I, J, model.grid.kx_map)
                    if !isnothing(K) && J[1] >= 1 && J[1] != I[1]
                        coupling_data[j] = zonal_mode_coupling(model.spectrum[I],
                                                    model.spectrum[J],
                                                    model.grid,
                                                    model.geometry,
                                                    model.problem)
                    else
                        coupling_data[j] = (zero(T), CouplingData(I, J, CartesianIndex(0, 0); FloatType = T))
                    end
                end

@debug "Rank $rank finished, sending $rank to head with tag $(comm_size + rank)"

                MPI.send(rank, comm; dest = 0, tag = comm_size + rank) 

@debug "Rank $rank sending $I to head with tag $(2 * comm_size + rank)"

                MPI.send(I, comm; dest = 0, tag = 2 * comm_size + rank)
                _, max_coupling_index = findmin(i -> i[1], coupling_data)
                MPI.send(coupling_data[max_coupling_index], comm; dest = 0, tag = 3 * comm_size + rank)
            end
        end
    end
end

function non_zonal_coupling(model::FluidModel{N, T},
                            comm::MPI.Comm;
                           ) where {N, T}
    rank = MPI.Comm_rank(comm)
    comm_size = MPI.Comm_size(comm)
    n_kx = n_kx_modes(model.grid, neg_kx = false)
    n_ky = n_ky_modes(model.grid)

    if rank == 0
        max_couplings = Matrix{CouplingData{T}}(undef, n_kx, n_ky)
        summed_couplings = Matrix{T}(undef, n_kx, n_ky)
        rank_status = fill(true, comm_size - 1)
        indices = CartesianIndices((max_couplings))
        I, S = nothing, nothing
        for i in eachindex(rank_status)
            try
                I, S = !isnothing(S) ? iterate(indices, S) : iterate(indices)
            catch
                I, S = CartesianIndex(-1, -1), S
            end
            MPI.send(I, comm; dest = i, tag = i)
            if I == CartesianIndex(-1, -1)
                rank_status[i] = false 
            end
        end

        finished = all(r -> !r, rank_status) 
        while !finished
            r = MPI.recv(comm; source = MPI.ANY_SOURCE, tag = MPI.ANY_TAG)
            J = MPI.recv(comm; source = r, tag = 2 * comm_size + r)

@debug "Head received $J from rank $r"

            c_data  = MPI.recv(comm; source = r, tag = 3 * comm_size + r)

@debug "Head received coupling data from rank $r"

            summed_couplings[J] = c_data[1]
            max_couplings[J] = c_data[2]
println("Received ", c_data[1], " at index ", J)
            try
                I, S = !isnothing(S) ? iterate(indices, S) : iterate(indices)
            catch
                I, S = CartesianIndex(-1, -1), S
            end

@debug "Head sending $I to rank $r with tag $r"

            MPI.send(I, comm; dest = r, tag = r)
            if I == CartesianIndex(-1, -1)
                rank_status[r] = false 
            end
            if all(i -> !i, rank_status)
                finished = true
            end
        end
        return summed_couplings, StructArray(max_couplings)
    else
        I = CartesianIndex(0, 0)
        coupling_data = Matrix{Tuple{T, CouplingData{T}}}(undef, n_kx_modes(model.grid), n_ky)
        while I != CartesianIndex(-1, -1)
            I = MPI.recv(comm; source = 0, tag = rank)

@debug "Rank $rank received $I from head with tag $rank"

            if I != CartesianIndex(-1, -1)
                 for J in CartesianIndices(model.spectrum)
                    K = triplet_matching(I, J, model.grid.kx_map)
                    if !isnothing(K) 
                        @debug "Evaluating triplet $I, $J, $K"
                        if K[2] != 0 && (K != J || K != I)
                            coupling_data[J] = non_zonal_coupling(model.spectrum[I],
                                                                  model.spectrum[J],
                                                                  model.spectrum[K],
                                                                  model.grid,
                                                                  model.geometry,
                                                                  model.problem)
                        else
                            coupling_data[J] = (zero(T), CouplingData(I, J, CartesianIndex(0, 0); FloatType = T))
                        end
                    else
                        coupling_data[J] = (zero(T), CouplingData(I, J, CartesianIndex(0, 0); FloatType = T))
                    end
                end

@debug "Rank $rank finished, sending $rank to head with tag $(comm_size + rank)"

                MPI.send(rank, comm; dest = 0, tag = comm_size + rank) 

@debug "Rank $rank sending $I to head with tag $(2 * comm_size + rank)"

                MPI.send(I, comm; dest = 0, tag = 2 * comm_size + rank)
                _, max_coupling_index = findmin(i -> i[1], coupling_data)
                MPI.send(coupling_data[max_coupling_index], comm; dest = 0, tag = 3 * comm_size + rank)
            end
        end
    end
end


function non_zonal_τC(eq::E,
                      comm::MPI.Comm,
                      normalization::T = 1.0,
                      scale_factor::T = 1.0;
                      kwargs...
                     ) where {T, E <: AbstractGeometry}
    rank = MPI.Comm_rank(comm)
    model = setup_linear_model(eq; kwargs...)
    if rank == 0
        compute_linear_spectrum!(model, comm)
@debug "Comm $comm computed linear spectrum"
        broadcast_spectrum!(model, 0, comm)
        c, max_c =non_zonal_coupling(model, comm)
@debug "Comm $comm computed couplings" 
        #τC_sum = sum(exp.(-c))
        τC_sum = -sum(min.(c, 0))
        model = 0
        return 1.0/((τC_sum)^scale_factor)
    else
        compute_linear_spectrum!(model, comm)
        broadcast_spectrum!(model, 0, comm)
        non_zonal_coupling(model, comm)
        model = 0
        return nothing
    end
end

function τC(eq::E,
            comm::MPI.Comm,
            normalization::NTuple{2, T} = (1.0, 1.0);
            kwargs...
           ) where {T, E <: AbstractGeometry}
    rank = MPI.Comm_rank(comm)
    model = setup_linear_model(eq; kwargs...)
    if rank == 0
        compute_linear_spectrum!(model, comm)
        for I in CartesianIndices((1:n_kx_modes(model.grid, neg_kx = false), 1:n_ky_modes(model.grid)))
            println(I, ' ', model.spectrum[I][1][1])
        end
@debug "Comm $comm computed linear spectrum"
        broadcast_spectrum!(model, 0, comm)
        #c_zf, _ = zonal_coupling(model, comm)
        c_nzf, _ = non_zonal_coupling(model, comm)
@debug "Comm $comm computed couplings" 
        #τC_sum = sum(exp.(-c))
        #τC_zf_sum = -sum(min.(c_zf, 0))
        τC_nzf_sum = -sum(min.(c_nzf, 0))
        #println("τC_zf_sum: ", '\t', τC_zf_sum)
        println("τC_nzf_sum: ", '\t', τC_nzf_sum)
        model = nothing
        #return normalization[1] / τC_zf_sum + normalization[2] / τC_nzf_sum
        return normalization[2] / τC_nzf_sum
    else
        compute_linear_spectrum!(model, comm)
        broadcast_spectrum!(model, 0, comm)
        #zonal_coupling(model, comm)
        non_zonal_coupling(model, comm)
        model = nothing
        return nothing
    end
end

function broadcast_spectrum!(model::FluidModel{N, T},
                             root::Int,
                             comm::MPI.Comm;
                            ) where {N, T}
    rank = MPI.Comm_rank(comm)
    for I in eachindex(model.spectrum)
        if rank == root
            MPI.bcast(model.spectrum[I], comm; root = root)
        else
            data = MPI.bcast(nothing, comm; root = root)
            model.spectrum[I] = data
        end
    end
    return nothing
end

function quasilinear_flux(surface::E,
                          comm::MPI.Comm,
                          normalization::T = 1.0;
                          kwargs...
                         ) where {T, E <: AbstractGeometry}
    rank = MPI.Comm_rank(comm)
    model = setup_linear_model(surface; kwargs...)
    if rank == 0
        compute_linear_spectrum!(model, comm)
        ql_flux = zero(T)
        for I in CartesianIndices(model.spectrum)
            for ev in n_ev(model.spectrum[I])
                ql_flux += model.spectrum[I][ev].amplitude * imag(model.spectrum[I][ev][1]) / model.spectrum[I][ev].k².perp
            end
        end
        return ql_flux / normalization
    else
        compute_linear_spectrum!(model, comm)
    end
end
#=
function compute_linear_spectrum!(model::FluidModel{N, T},
                                  comm::MPI.Comm;
                                 ) where {N, T}
    rank = MPI.Comm_rank(comm)
    comm_size = MPI.Comm_size(comm)
    n_kx = n_kx_modes(model.grid, neg_kx = false)
    n_ky = n_ky_modes(model.grid)
    n_points = model.problem.n_points
    if comm_size < n_kx
        kx_spill = [0]
        columns_per_proc, diff = divrem(n_kx, comm_size)
        kx_column = rank <= diff ? 
    kx_spill, kx_column = Vector.(divrem(rank, comm_size))
    
    if kx_spill[] == 0
    end

end
=#
