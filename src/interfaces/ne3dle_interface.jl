using .NE3DLE

"""
For using NE3DLE, run surface = call_NE3DLE("path_to/NE3DLE.in").
The NE3DLESurface will be the equilibrium information for setupLinearModel().
"""
function setupLinearModel(surface::NE3DLE.NE3DLESurface;
                          nFields=3,
                          nVectors=1,
                          prec=Float64,
                          nkx=11,
                          nky=10,
                          Δkx = 0.05,
                          Δky = 0.05,
                          kxStart = 0.0,
                          kyStart = 0.0,
                          kxSpecLimit = 0.2,
                          kySpecLimit = 0.6,
                          kxSpecPower = 2.0,
                          kySpecPower = 4/3,
                          ∇T = 3.0,
                          ∇n = 0.0,
                          τ = 1.0,
                          β = 1.0,
                          solver=JDQZ(),
                          solnInterval=8π,
                          ε=200.0,
                          errorOrder=4,
                          hyperdiffusionOrder=2,
                          quadPoints=1000,
                          #points_per_2pi=128, #This is determined in NE3DLE.in
                         )
    gene = NE3DLE.NE3DLEFieldLine(surface).gene
    iota = gene.iota;
    nfp = gene.nfp;

    plasma = PlasmaParameters(∇T,∇n,τ,β)
    specGrid = SpectralGrid(nkx=nkx,nky=nky,Δkx=Δkx,Δky=Δky,
                            kxStart=kxStart,kyStart=kyStart,
                            kxSpecLimit=kxSpecLimit,kySpecLimit=kySpecLimit,
                            kxSpecPower=kxSpecPower,kySpecPower=kySpecPower)
    problem = LinearProblem(solver=solver,solnInterval=solnInterval,
                            ε=ε,errorOrder=errorOrder,
                            hyperdiffusionOrder=hyperdiffusionOrder,
                            quadPoints=quadPoints)


    #NE3DLE.extend_flux_tube!() uses angles in units of π
    if surface.params.mapping == "miller"
        s_hat = gene.shat/(surface.norms.ρ*surface.norms.B_ref/surface.norms.dpsi_drho)
        maxAngle = (last(specGrid.kxRange)/(abs(s_hat)*first(specGrid.kyRange)) + solnInterval)
        gene_ext = NE3DLE.extend_flux_tube(maxAngle/π,gene,surface.norms)
    else
        s_hat = gene.shat
        maxAngle = (last(specGrid.kxRange)/(abs(s_hat)*first(specGrid.kyRange)) + solnInterval)
        gene_ext = NE3DLE.extend_flux_tube(maxAngle/π,gene)
    end

    tubeLength = length(gene_ext.gxx)

    metric = Vector{NTuple{6, Float64}}(undef,tubeLength)

    for i in 1:tubeLength
        metric[i] = (gene_ext.gxx[i],gene_ext.gxy[i],gene_ext.gyy[i],0,0,0)
    end

    geom = FieldlineGeometry(metric,gene_ext.Bhat,gene_ext.jac,gene_ext.K1,gene_ext.K2,gene_ext.∂B∂θ,gene_ext.θ*π)
    geomPars = GeneParameters(iota,s_hat,nfp,vector2range(gene_ext.θ*π))

    return FluidModel(nFields,nVectors,plasma,specGrid,problem,geom,geomPars,prec=prec)

end
