using .VMEC
using .GeneTools

"""
    setup_linear_model(surface::VmecSurface; kwargs...)

Sets up a linear model from a VmecSurface type with arguments defined by `kwargs`.

# Arguments
- `n_fields`: # Number of fluid model fields
"""
function setup_linear_model(surface::VmecSurface;
                            n_fields=3,
                            n_ev=1,
                            prec=Float64,
                            nkx=11,
                            nky=10,
                            Δkx = 0.05,
                            Δky = 0.05,
                            kxSpecLimit = 0.2,
                            kySpecLimit = 0.6,
                            kxSpecPower = 2.0,
                            kySpecPower = 4/3,
                            ∇T = 3.0,
                            ∇n = 0.0,
                            τ = 1.0,
                            β = 1.0,
                            solver=ArnoldiMethod(),
                            soln_interval=8π,
                            ε=200.0,
                            errorOrder=4,
                            hyperdiffusionOrder=2,
                            quadPoints=1024,
                            n_points = 1024,
                           )
    iota = surface.iota[1];
    s_hat = VMEC.shat(surface);
    nfp = surface.nfp;

    plasma = PlasmaParameters(∇T,∇n,τ,β, FloatType = prec)
    grid = SpectralGrid(nkx=nkx,nky=nky,Δkx=Δkx,Δky=Δky,
                        kxSpecLimit=kxSpecLimit,kySpecLimit=kySpecLimit,
                        kxSpecPower=kxSpecPower,kySpecPower=kySpecPower,
                        FloatType = prec)
    
    problem = LinearProblem(solver=solver, n_ev = n_ev, n_points = n_points,
                            soln_interval=soln_interval, ε=ε,error_order=errorOrder,
                            hyper_diffusion_order=hyperdiffusionOrder,
                            quad_points=quadPoints, FloatType = prec)
    setup_sparse_operators!(problem)
    nkx2 = n_kx_modes(grid; neg_kx = false)

    max_θ = grid.k_perp.kx[nkx2, 1]/(abs(s_hat)*grid.k_perp.ky[nkx2, 1]) + soln_interval
    n_fieldline_points = convert(Int, ceil(2 * max_θ * n_points / soln_interval))

    # Defined in θ
    θ_range = range(start = -max_θ, stop = max_θ, length = n_fieldline_points)
    vector2range(θ_range)

    p = MagneticCoordinateCurve(PestCoordinates,surface.s*surface.phi[end]/(2π)*surface.signgs,0.0,-θ_range/iota[1])

    metric, modB, sqrtg, K1, K2, ∂B∂θ = gene_geometry_coefficients(GeneFromPest(),p,surface)
    #θ = map(i->-i.ζ*iota,p)
    geom = FieldlineGeometry(metric, modB, sqrtg, K1, K2, ∂B∂θ, θ_range; ι = iota, s_hat = s_hat, nfp = nfp, FloatType = prec)

    return FluidModel(plasma, grid,problem,geom; n_fields = n_fields, n_ev = n_ev, FloatType=prec, field_labels = (:Φ, :U, :T))

end

