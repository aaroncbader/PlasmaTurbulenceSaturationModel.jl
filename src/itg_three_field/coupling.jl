function coupling_matrix(k_perp::SpectralPoint{T},
                         bounds::BoundsTuple{T},
                         eigenvalues::SVector{3, Complex{T}},
                         eigenvectors::Vector{SplineType},
                         problem::LinearProblem{3, S, T, E},
                         plasma::PlasmaParameters{T},
                        ) where {S <: AbstractSolverMethod, T, E}
  ky = k_perp.ky
  η_range = range(start = bounds.min, stop = bounds.max, length = problem.n_points)
  if step(η_range) <= zero(T)
    η_range = StepRangeLen(first(η_range), eps(T), length(η_range))
  end
  itp(A) = scale(interpolate(A, BSpline(Cubic(Throw(OnGrid())))), η_range)
  ηi = plasma.∇T - 2/3*plasma.∇n
  τ = plasma.τ
  m₁₁ = 1.0
  m₂₁ = ky*ηi/eigenvalues[1]
  m₂₂ = ky*ηi/eigenvalues[2]
  m₂₃ = ky*ηi/eigenvalues[3]
  m₃₁ = im/eigenvalues[1]*(1+5/3*τ+τ*ky*ηi/eigenvalues[1])
  m₃₂ = im/eigenvalues[2]*(1+5/3*τ+τ*ky*ηi/eigenvalues[2])
  m₃₃ = im/eigenvalues[3]*(1+5/3*τ+τ*ky*ηi/eigenvalues[3])
  mass_mat = [m₁₁ m₁₁ m₁₁;
              m₂₁ m₂₂ m₂₃;
              m₃₁ m₃₂ m₃₃]
  mass_mat_inv = inv(mass_mat)
  M11 = itp(fill(m₁₁, problem.n_points))
  M21 = itp(fill(m₂₁, problem.n_points))
  M22 = itp(fill(m₂₂, problem.n_points))
  M23 = itp(fill(m₂₃, problem.n_points))
  M31 = itp(fill(m₃₁, problem.n_points))
  M32 = itp(fill(m₃₂, problem.n_points))
  M33 = itp(fill(m₃₃, problem.n_points))

  M⁻¹₁₁ = itp(fill(mass_mat_inv[1, 1], problem.n_points))
  M⁻¹₁₂ = itp(fill(mass_mat_inv[1, 2], problem.n_points))
  M⁻¹₁₃ = itp(fill(mass_mat_inv[1, 3], problem.n_points))
  M⁻¹₂₁ = itp(fill(mass_mat_inv[2, 1], problem.n_points))
  M⁻¹₂₂ = itp(fill(mass_mat_inv[2, 2], problem.n_points))
  M⁻¹₂₃ = itp(fill(mass_mat_inv[2, 3], problem.n_points))
  M⁻¹₃₁ = itp(fill(mass_mat_inv[3, 1], problem.n_points))
  M⁻¹₃₂ = itp(fill(mass_mat_inv[3, 2], problem.n_points))
  M⁻¹₃₃ = itp(fill(mass_mat_inv[3, 3], problem.n_points)) 

  return (SMatrix{3, 3}([M11, M21, M31, M11, M22, M23, M11, M32, M33]),
          SMatrix{3, 3}([M⁻¹₁₁, M⁻¹₂₁, M⁻¹₃₁, M⁻¹₁₂, M⁻¹₂₂, M⁻¹₂₃, M⁻¹₁₃, M⁻¹₃₂, M⁻¹₃₃]))
end

function zonal_mode_coupling(model::FluidModel{3, T};
                            ) where {T}
    n_kx = n_kx_modes(model.grid, neg_kx = false)
    n_ky = n_ky_modes(model.grid)
    coupling_data = Vector{Tuple{T, CouplingData{T}}}(undef, n_kx)
    max_couplings = Matrix{CouplingData{T}}(undef, n_kx, n_ky)
    summed_couplings = zeros(n_kx, n_ky)
    for I in CartesianIndices((1:n_kx, 1:n_ky))
        for j in 1:n_kx
            J = CartesianIndex(j, I[2])
            K = triplet_matching(I, J, model.grid.kx_map)
            if !isnothing(K) && J[1] >= 1 && J[1] != I[1]
                coupling_data[j] = zonal_mode_coupling(model.spectrum[I],
                                                       model.spectrum[J],
                                                       model.grid,
                                                       model.geometry,
                                                       model.problem)
            else
                coupling_data[j] = (zero(T), CouplingData(I, J, CartesianIndex(0, 0); FloatType = T))
            end
        end
        _, max_coupling_index = findmin(i -> i[1], coupling_data)
        max_couplings[I] = coupling_data[max_coupling_index][2]
        summed_couplings[I] = coupling_data[max_coupling_index][1]
    end
    return summed_couplings, StructArray(max_couplings)
end

function zonal_mode_coupling(bundle_1::DriftWaveBundle{3, T, E, Fields},
                             bundle_2::DriftWaveBundle{3, T, E, Fields},
                             grid::SpectralGrid{T},
                             geom::FieldlineGeometry{T},
                             problem::LinearProblem{3, S, T, E};
                            ) where {S, T, E, Fields}

    max_couplings = Vector{CouplingData{T}}(undef, E^2)
    summed_couplings = zero(T)
    for (i, mode_1) in enumerate(bundle_1.modes) 
        for (j, mode_2) in enumerate(bundle_2.modes)
            m = E * (i - 1) + j
            sum_c, max_c = zonal_mode_coupling(mode_1, mode_2,
                                               grid, geom, problem; ev_tuple = (Int8(i), Int8(j)))
            max_couplings[m] = max_c
            summed_couplings += sum_c
        end
    end
    _, max_coupling_index = findmax(c -> abs2(c.value), max_couplings)
    return summed_couplings, max_couplings[max_coupling_index]
end

function zonal_mode_coupling(mode_1::DriftWave{3, T, Fields},
                             mode_2::DriftWave{3, T, Fields},
                             grid::SpectralGrid{T},
                             geom::FieldlineGeometry{T},
                             problem::LinearProblem{3, S, T, E};
                             ev_tuple::NTuple{2, Int} = (1, 1),
                            ) where {S, T, E, Fields}
    (kx1, ky1) = (mode_1.k_perp.kx, mode_1.k_perp.ky)
    (kx2, ky2) = (mode_2.k_perp.kx, mode_2.k_perp.ky)
    overlap_points = overlap_interval(mode_1.η_span, mode_2.η_span)
    K = triplet_matching(mode_1.I, mode_2.I, grid.kx_map)
    if !isnothing(overlap_points) && grid.kx_map.forward[K[1]] < 0
        θ = 0.5 * (overlap_points.min + overlap_points.max)
        τ = -im/(mode_2[2] - conj(mode_1[1]))
        C = ky1 * (kx1 - kx2) * (mode_1.inv_coupling_matrix[1,2](θ) *
                                 mode_2.coupling_matrix[2,2](θ) +
                                 mode_1.inv_coupling_matrix[1,3](θ) *
                                 mode_2.coupling_matrix[3,2](θ))
        overlap_value = overlap_integral(overlap_points, mode_1.β[1], mode_2.β[2], geom, problem)
        amp = (mode_1.amplitude * mode_2.amplitude * power_spectrum_coeff(CartesianIndex(K[1], 1), grid))^2
        overlap = overlap_value / problem.soln_interval
        energy_damping_coeff =  -min(imag(mode_2[2]), zero(T)) / (imag(mode_1[1]) + max(imag(mode_2[2]), zero(T))) 
        coupling = energy_damping_coeff * imag(mode_1[1]) / mode_1.k².perp * real(τ * C * overlap * amp)
        @debug coupling, mode_1[1], τ, C, overlap, amp
        return coupling, CouplingData{T}(τ, C, overlap, coupling, 
                                         (Int8.(Tuple(mode_1.I)), Int8.(Tuple(mode_2.I)), Int8.(Tuple(K))), 
                                         Int8.(ev_tuple[1], ev_tuple[2], 0), Int8.(1, 2, 3))
    else
        return zero(T), CouplingData(mode_1.I, mode_2.I, CartesianIndex(0, 0); FloatType = T) 
    end
end
            
function non_zonal_coupling(model::FluidModel{3, T};
                            ) where {T}
    n_kx = n_kx_modes(model.grid, neg_kx = false)
    n_ky = n_ky_modes(model.grid)
    coupling_data = Matrix{Tuple{T, CouplingData{T}}}(undef, n_kx_modes(model.grid), n_ky)
    max_couplings = Matrix{CouplingData{T}}(undef, n_kx, n_ky)
    summed_couplings = zeros(n_kx, n_ky)
    for I in CartesianIndices((1:n_kx, 1:n_ky))
@debug I
        #@batch for J in CartesianIndices(model.spectrum)
        for J in CartesianIndices(model.spectrum)
            K = triplet_matching(I, J, model.grid.kx_map)
            if !isnothing(K)
                #@debug "Evaluating triplet $I, $J, $K"
                if K[2] != 0 && (K != I || K != J)
                    coupling_data[J] = non_zonal_coupling(model.spectrum[I],
                                                          model.spectrum[J],
                                                          model.spectrum[K],
                                                          model.grid,
                                                          model.geometry,
                                                          model.problem)
                else
                    coupling_data[J] = (zero(T), CouplingData(I, J, CartesianIndex(0, 0); FloatType = T))
                end
            else
                coupling_data[J] = (zero(T), CouplingData(I, J, CartesianIndex(0, 0); FloatType = T))
            end
        end
        _, max_coupling_index = findmin(i -> i[1], coupling_data)
        max_couplings[I] = coupling_data[max_coupling_index][2]
        summed_couplings[I] = coupling_data[max_coupling_index][1]
    end
    return summed_couplings, StructArray(max_couplings)
end
            

function non_zonal_coupling(bundle_1::DriftWaveBundle{3, T, E, Fields},
                            bundle_2::DriftWaveBundle{3, T, E, Fields},
                            bundle_3::DriftWaveBundle{3, T, E, Fields},
                            grid::SpectralGrid{T},
                            geom::FieldlineGeometry{T},
                            problem::LinearProblem{3, S, T, E};
                           ) where {S, T, E, Fields}
    coupling_data = Vector{Tuple{T, CouplingData{T}}}(undef, E^3)
    max_couplings = Vector{CouplingData{T}}(undef, E^3)
    summed_couplings = zero(T)
    @inbounds for m in eachindex(coupling_data)
        d1, r1 = divrem(m - 1, E * E)
        d2, r2 = divrem(r1, E)
        i = d1 + 1
        j = d2 + 1
        k = r2 + 1
        coupling_data[m] = non_zonal_coupling(bundle_1[i], bundle_2[j], bundle_3[k],
                                              grid, geom, problem;
                                              ev_tuple = (i ,j, k))
    end
    for m in eachindex(coupling_data, max_couplings)
        max_couplings[m] = coupling_data[m][2]
        summed_couplings += coupling_data[m][1]
    end
    _, max_coupling_index = findmin(c -> c.value, max_couplings)
    return summed_couplings, max_couplings[max_coupling_index]  
end

function non_zonal_coupling(mode_1::DriftWave{3, T, Fields},
                            mode_2::DriftWave{3, T, Fields},
                            mode_3::DriftWave{3, T, Fields},
                            grid::SpectralGrid{T},
                            geom::FieldlineGeometry{T},
                            problem::LinearProblem{3, S, T, E};
                            ev_tuple::NTuple{3, Int} = (1, 1, 1),
                           ) where {S, T, E, Fields}
    (kx1, ky1) = (mode_1.k_perp.kx, mode_1.k_perp.ky)
    (kx2, ky2) = (mode_2.k_perp.kx, mode_2.k_perp.ky)
    (kx3, ky3) = (mode_3.k_perp.kx, mode_3.k_perp.ky)
    unique_combinations = Vector{Vector{Int}}(undef, 27)
    for i in eachindex(unique_combinations)
        p = div(i -1 , 9) + 1
        q = div(rem(i-1, 9), 3) + 1
        r = rem(rem(i-1, 9), 3) + 1
        unique_combinations[i] = [p, q, r]
    end
    filter!(c -> c[1] == 1 && (c[2] == 2 || c[3] == 2), unique_combinations)
    τ = zeros(Complex{T}, length(unique_combinations))
    C = zeros(Complex{T}, length(unique_combinations))
    overlap = zeros(Complex{T}, length(unique_combinations))
    couplings = zeros(T, length(unique_combinations))
    coupling_combos = Vector{NTuple{3, Int}}(undef, length(unique_combinations))
    for (i, c) in enumerate(unique_combinations)
        coupling_combos[i] = Tuple(c)
        (p_index, p) = (c[1], mode_1[c[1]])
        (s_index, s) = (c[2], mode_2[c[2]])
        (t_index, t) = (c[3], mode_3[c[3]])
        τ[i] = -im/(t + s - conj(p))
        overlap_points = overlap_interval(mode_1.η_span, mode_2.η_span, mode_3.η_span)
        if !isnothing(overlap_points)
            θ = 0.5 * (overlap_points.min + overlap_points.max)
            C[i] = (kx1 * ky2 - ky1 * kx2)*(mode_1.inv_coupling_matrix[p_index, 2](θ) *
                                           mode_2.coupling_matrix[2, s_index](θ) +
                                           0.5 * mode_1.inv_coupling_matrix[p_index,3](θ) *
                                           mode_2.coupling_matrix[3, s_index](θ))
            overlap[i] = overlap_integral(overlap_points, mode_1.β[1], mode_2.β[1],
                                          mode_3.β[1], geom, problem)
        end
        amp = (mode_1.amplitude * mode_2.amplitude * mode_3.amplitude)^2
        #=
        energy_injection_coeff = imag(p) * 
                                  (imag(s) > zero(T) ? imag(s) : zero(T) + 
                                  imag(t) > zero(T) ? imag(t) : one(T)) /
                                  (imag(s) < zero(T) ? -imag(s) : zero(T) + 
                                   imag(t) < zero(T) ? -imag(t) : one(T))
        =#
        energy_damping_coeff =  -(min(imag(s), zero(T)) + min(imag(t), zero(T))) /
                                  (imag(p) + max(imag(s), zero(T)) + max(imag(t), zero(T))) 

        #couplings[i] = energy_damping_coeff * real(τ[i] * C[i] * overlap[i] * amp) * imag(p) / mode_1.k².perp
        couplings[i] = real(τ[i])
        println("(", kx1, ", ", ky1, "), (", kx2, ", ", ky2, "): ,(", kx3, ", ", ky3, "): ", couplings[i])
        println(p, " ", s, " ", t)
    end
    _, max_coupling_index = findmin(couplings)
    #return (sum(couplings),
    return (couplings[max_coupling_index],
            CouplingData{T}(τ[max_coupling_index], C[max_coupling_index], 
                            overlap[max_coupling_index], couplings[max_coupling_index],
                            (Int8.(Tuple(mode_1.I)), Int8.(Tuple(mode_2.I)), Int8.(Tuple(mode_3.I))),
                            Int8.(ev_tuple), Int8.(coupling_combos[max_coupling_index])))
end
