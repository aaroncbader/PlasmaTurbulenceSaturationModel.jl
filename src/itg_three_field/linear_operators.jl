

function setup_sparse_operators!(problem::LinearProblem{3, S, T, E};
                                ) where {S <: ArnoldiMethod, T, E}
    n_points = problem.n_points
    error_order = problem.derivative_error_order - rem(problem.derivative_error_order, 2)
    fd_method = parallel_derivative(div(n_points, 2), n_points, 1, error_order)

    fdm_stencils = zeros(Int, length(fd_method.grid), error_order + 1) 
    hyp_diff_stencils = zeros(Int, length(fd_method.grid), error_order + 1) 
    for i in axes(fdm_stencils, 2)
        j = i <= div(error_order, 2) ? i : i >= error_order ? 
            n_points - error_order - 1 + i : div(n_points, 2)
        grid_points = (parallel_derivative(j, n_points, 1, error_order)).grid
        fdm_stencils[:, i] .= grid_points
        grid_points = (parallel_derivative(j, n_points, problem.hyper_diffusion_order, error_order)).grid
        hyp_diff_stencils[:, i] .= grid_points
    end

    lhs_output = Matrix{Complex{T}}(undef, 3 * problem.n_points, 3 * problem.n_points)

    #function lhs_operator(output,
    #                      input;
    #                     )

        for index in 1:n_points
            j = (index <= div(error_order, 2) ? index : index >= (n_points - div(error_order, 2)) ? 
                error_order + 1 - (n_points - index) : div(error_order, 2) + 1)
            jr = range(start = j > div(error_order, 2) ? 1 : 2, stop = j < error_order ? error_order + 1 : error_order)
            
            lhs_output[index, index .+ fdm_stencils[jr, j]] .= one(T)
            lhs_output[index, (n_points + index) .+ fdm_stencils[jr, j]] .= one(T) 
            lhs_output[index, (2 * n_points + index) .+ fdm_stencils[jr, j]] .= one(T) #sum(input[index .+ fdm_stencils[jr, j]] + input[(n_points + index) .+ fdm_stencils[jr, j]] + input[(2 * n_points + index) .+ fdm_stencils[jr, j]]) 

            lhs_output[n_points + index, index .+ fdm_stencils[jr, j]] .= one(T)
            lhs_output[n_points + index, (n_points + index) .+ fdm_stencils[jr, j]] .= one(T)
            lhs_output[n_points + index, (2 * n_points + index) .+ fdm_stencils[jr, j]] .= one(T) #sum(input[index .+ fdm_stencils[jr, j]] + input[(n_points + index) .+ fdm_stencils[jr, j]] + input[(2 * n_points + index) .+ fdm_stencils[jr, j]])
            
            lhs_output[2 * n_points + index, index] = one(T)
            lhs_output[2 * n_points + index, (2 * n_points + index) .+ fdm_stencils[jr, j]] .= one(T) #input[index] + sum(input[(2 * n_points + index) .+ fdm_stencils[jr, j]])

        end 
    #end

    #function rhs_operator(output, input)
    rhs_output = Matrix{Complex{T}}(undef, 3 * problem.n_points, 3 * problem.n_points)
        for index in 1:n_points
            rhs_output[index, index] = one(T)
            rhs_output[index, 2 * n_points + index] = one(T) #input[index] + input[2 * n_points + index]
            rhs_output[n_points + index, n_points + index] = one(T) #input[n_points + index]
            rhs_output[2 * n_points + index, 2 * n_points + index] = one(T) #input[2 * n_points + index]
        end
    #end

    input = rand(Complex{T}, 3 * problem.n_points)
    #lhs_output = Matrix{Complex{T}}(undef, 3 * problem.n_points, 3 * problem.n_points)
    #FiniteDiff.finite_difference_jacobian!(lhs_output, lhs_operator, input)
    lhs = Complex{T}.(sparse(lhs_output))
    lhs_output = nothing
    colors = matrix_colors(lhs)
    cache = FiniteDiff.JacobianCache(input, colorvec = colors, sparsity = lhs)
    problem.lhs.mat = lhs
    problem.lhs.colors = colors
    problem.lhs.cache = cache

    #rhs_output = Matrix{Complex{T}}(undef, 3 * problem.n_points, 3 * problem.n_points)
    #FiniteDiff.finite_difference_jacobian!(rhs_output, rhs_operator, input)
    rhs = Complex{T}.(sparse(rhs_output))
    rhs_output = nothing
    colors = matrix_colors(rhs)
    cache = FiniteDiff.JacobianCache(input, colorvec = colors, sparsity = rhs)
    problem.rhs.mat = rhs
    problem.rhs.colors = colors
    problem.rhs.cache = cache

    return nothing
end
#=
function setup_sparse_operators!(problem::LinearProblem{3, S, T, E};
                                ) where {S <: ArnoldiMethod, T, E}

    n_points = problem.n_points
    rank = 3 * n_points
    lhs = zeros(Complex{T}, (rank, rank))
    rhs = Matrix{Complex{T}}(I, (rank, rank))

    @inbounds for index in 1:n_points

        # Compute the finite difference splines for the first order derivative operator
        # e.g. ∂/∂z → [c₁,c₂,…,cₙ₋₁,cₙ] where n = model.grid.derivative_error_order + 1
        fdm = parallel_derivative(index, n_points, 1, problem.derivative_error_order)
        # Compute the finite difference spline for the n-th order derivative operator
        # where n is specified by model.grid.hyper_diffusion_order
        hyper_diff = parallel_derivative(index, n_points,
                                            problem.hyper_diffusion_order,
                                            problem.derivative_error_order)

        # Deal with boundaries where the point beyond the range is assumed to be zero
        deriv_indices = findall(x -> ((x + index) > 0 && (x + index) <= n_points), fdm.grid)
        hyper_diff_indices = findall(x -> ((x + index) > 0 && (x + index) <= n_points), hyper_diff.grid)

        fdm_grid = fdm.grid[deriv_indices]
        # Apply the prefactor -i/√gB to each derivative term

        hyper_diff_grid = hyper_diff.grid[hyper_diff_indices]
        # Approximate the n-th order hyperdiffusion operator by taking only the
        # leading order term

        # Use array views to efficiently access the underlying data to apply the
        # finite differnce splines. For the three-field model with operator rank
        # 3N × 3N, the system can be broken into N × N blocks:
        #     || Φₖ ||    || A1 | A2 | A3 ||   || Φₖ ||
        # A ⋅ || Uₖ || ⇒  || A4 | A5 | A6 || ⋅ || Uₖ ||
        #     || Tₖ ||    || A7 | A8 | A9 ||   || Tₖ ||

        # Left-hand side operator
        # Block 1
        lhsView = view(lhs, index, index .+ hyper_diff_grid)
        lhsView .+= 1
        lhs[index, index] += 1

        # Block 2
        lhsView = view(lhs, index, (n_points+index) .+ fdm_grid)
        lhsView .+= 1

        # Block 3
        lhsView = view(lhs, index, (2 * n_points + index) .+ fdm_grid)
        lhsView .+= 1
        
        # Block 4
        lhsView = view(lhs,n_points+index,index .+ fdm_grid)
        lhsView .+= 1

        # Block 5
        lhsView = view(lhs, n_points + index, (n_points + index) .+ hyper_diff_grid)
        lhsView .+= 1

        # Block 6
        lhsView = view(lhs, n_points + index,(2 * n_points + index) .+ fdm_grid)
        lhsView .+= 1

        # Block 7
        lhs[2 * n_points + index, index] += 1 #+ 10*τ*Dk_θ/9
        #lhs[2*nPoints+index,2*nPoints+index] += 5*τ*Dk_θ/3

        # Block 8
        #
        # Block 9
        lhsView = view(lhs, 2 * n_points + index, (2 * n_points + index) .+ hyper_diff_grid)
        lhsView .+= 1

        # Right hand side operator
        # Block 1
        rhs[index, index] += 1

        #Block 3
        rhs[index, 2 * n_points + index] += 1

    end
    input = rand(Complex{T}, 3 * problem.n_points)
    lhs_output = Complex{T}.(sparse(lhs))
    colors = matrix_colors(lhs_output)
    cache = FiniteDiff.JacobianCache(input, colorvec = colors, sparsity = lhs_output)
    problem.lhs.mat = lhs_output
    problem.lhs.colors = colors
    problem.lhs.cache = cache

    rhs_output = Complex{T}.(sparse(rhs))
    colors = matrix_colors(rhs_output)
    cache = FiniteDiff.JacobianCache(input, colorvec = colors, sparsity = rhs_output)
    problem.rhs.mat = rhs_output
    problem.rhs.colors = colors
    problem.rhs.cache = cache

    return nothing
end
=#
    

"""
    build_linear_operators!(problem, dw, geom, plasma)

Build the linear operator
"""
function build_linear_operators!(problem::LinearProblem{3, S, T, E},
                                 dw::Union{DriftWave{3, T, Fields}, DriftWaveBundle{3, T, E, Fields}},
                                 geom::FieldlineGeometry{T},
                                 plasma::PlasmaParameters;
                                ) where {S <: ArnoldiMethod, T, E, Fields}
    n_points = problem.n_points
    error_order = problem.derivative_error_order - rem(problem.derivative_error_order, 2)
    fd_method = parallel_derivative(div(n_points, 2), n_points, 1, error_order)

    fdm_stencils = zeros(Int, length(fd_method.grid), error_order + 1) 
    fdm_coefs = zeros(Complex{T}, length(fd_method.grid), error_order + 1)
    hyp_diff_stencils = zeros(Int, length(fd_method.grid), error_order + 1) 
    hyp_diff_coefs = zeros(Complex{T}, length(fd_method.grid), error_order + 1)
    for i in axes(fdm_stencils, 2)
        j = i <= div(error_order, 2) ? i : i >= error_order ? 
            n_points - error_order - 1 + i : div(n_points, 2)
        deriv = parallel_derivative(j, n_points, 1, error_order)
        hyp = parallel_derivative(j, n_points, problem.hyper_diffusion_order, error_order)
        fdm_stencils[:, i] .= deriv.grid
        fdm_coefs[:, i] .= Complex{T}.(deriv.coefs)
        hyp_diff_stencils[:, i] .= hyp.grid
        hyp_diff_coefs[:, i] .= Complex{T}.(hyp.coefs)
    end

#=
    function lhs_operator(output,
                          input;
                         )

        for index in 1:n_points
            j = (index <= div(error_order, 2) ? index : index >= (n_points - div(error_order, 2)) ? 
                error_order + 1 - (n_points - index) : div(error_order, 2) + 1)
            jr = range(start = j > div(error_order, 2) ? 1 : 2, stop = j < error_order ? error_order + 1 : error_order)
            output[index] = input[2 * n_points + index] + sum(input[index .+ fdm_stencils[jr, j]] + input[(n_points + index) .+ fdm_stencils[jr, j]]) 

            output[n_points + index] = sum(input[index .+ fdm_stencils[jr, j]] + input[(n_points + index) .+ fdm_stencils[jr, j]] + input[(2 * n_points + index) .+ fdm_stencils[jr, j]])
            
            output[2 * n_points + index] = input[index] + sum(input[(2 * n_points + index) .+ fdm_stencils[jr, j]])

        end 
    end

    input = rand(Complex{T}, 3 * problem.n_points)
    FiniteDiff.finite_difference_jacobian!(problem.lhs.mat, lhs_operator, input, problem.lhs.cache) 
=#
    

    kx = dw.k_perp.kx
    ky = dw.k_perp.ky
      
    # For convenience extract the relevant plasma parameters
    ∇n = plasma.∇n
    ∇T = plasma.∇T
    τ = plasma.τ

    # Set the numerical hyperdiffusion parameters
    θ_range = range(start = dw.η_span.min, stop = dw.η_span.max, length = problem.n_points)
    Δθ = step(θ_range)
    rank = 3 * n_points

    hyper_diff_ε = problem.ε
    hyp_z_coeff = hyper_diff_ε*(im*0.5*Δθ)^problem.hyper_diffusion_order

    j = div(error_order, 2) + 1
    block_1 = Vector{Complex{T}}(undef, maximum(problem.lhs.colors))
    block_2 = similar(block_1)
    block_3 = similar(block_1)
    diag_pos = zeros(Complex{T}, error_order + 1)
    diag_pos[j] = one(Complex{T})
    fd_coeffs = fdm_coefs[:, j]
    fd_θ_coefs = similar(fd_coeffs)
    hyp_coeffs = hyp_diff_coefs[:, j]
    hyp_θ_coefs = similar(hyp_coeffs)

    @inbounds for index in j:(n_points - j) + 1
        θ = θ_range[index]
        Bk_θ, Dk_θ = damping_drive_term(θ, kx, ky, geom)
        B = geom.B(θ)
        jac = geom.jac(θ)
        fd_θ_coefs[:] .= (-im/(Δθ*jac*B) * fd_coeffs)
        hyp_θ_coefs[:] .= (-im * hyp_z_coeff/(Δθ * jac * B)^problem.hyper_diffusion_order)*hyp_coeffs
        σ = (1.0 /(1.0 + Bk_θ * (1 + 5 * τ/3)))
        ν = -(Bk_θ * τ /(1.0 + Bk_θ * (1 + 5 * τ/3)))


        block_1[:] .= [(hyp_θ_coefs .+ (ky * ∇n/B + Dk_θ * (1 + 5 * τ/3)) .* diag_pos)...,
                       fd_θ_coefs...,
                       ((Dk_θ * τ) .* diag_pos)...]
        for (i, r) in enumerate(rowvals(problem.lhs.mat[index, :]))
            problem.lhs.mat[index, r] = block_1[i]
        end
        problem.rhs.mat[index, index] = σ
        problem.rhs.mat[index, 2 * n_points + index] = ν

        block_2[:] .= [((1 + 5 * τ/3) * fd_θ_coefs)...,
                       hyp_θ_coefs...,
                       τ * fd_θ_coefs...]
        for (i, r) in enumerate(rowvals(problem.lhs.mat[n_points + index, :]))
            problem.lhs.mat[n_points + index, r] = block_2[i]
        end
        problem.rhs.mat[n_points + index, n_points + index] = one(Complex{T})

        nz = length(rowvals(problem.lhs.mat[2 * n_points + index, :]))
        block_3[1:nz] .= [ky * (∇T - 2 * ∇n/3)/B, hyp_θ_coefs...] 
        for (i, r) in enumerate(rowvals(problem.lhs.mat[2 * n_points + index, :]))
            problem.lhs.mat[2 * n_points + index, r] = block_3[i]
        end
        problem.rhs.mat[2 * n_points + index, 2 * n_points + index] = one(Complex{T})
    end 

        
    @inbounds for j in 1:div(error_order,2) 
        diag_pos = zeros(Complex{T}, error_order)
        diag_pos[j] = one(Complex{T})
        fd_coeffs = fdm_coefs[2:error_order+1, j]
        fd_θ_coefs = similar(fd_coeffs)
        hyp_coeffs = hyp_diff_coefs[2:error_order+1, j]
        hyp_θ_coefs = similar(hyp_coeffs)
        
        index = j
        θ = θ_range[index]
        Bk_θ, Dk_θ = damping_drive_term(θ, kx, ky, geom)
        B = geom.B(θ)
        jac = geom.jac(θ)
        fd_θ_coefs[:] .= (-im/(Δθ*jac*B) * fd_coeffs)
        hyp_θ_coefs[:] .= (-im * hyp_z_coeff/(Δθ * jac * B)^problem.hyper_diffusion_order)*hyp_coeffs
        σ = (1.0 /(1.0 + Bk_θ * (1 + 5 * τ/3)))
        ν = -(Bk_θ * τ /(1.0 + Bk_θ * (1 + 5 * τ/3)))


        nz = length(rowvals(problem.lhs.mat[index, :]))
        block_1[1:nz] .= [(hyp_θ_coefs .+ (ky * ∇n/B + Dk_θ * (1 + 5 * τ/3)) .* diag_pos)...,
                          fd_θ_coefs...,
                          ((Dk_θ * τ) .* diag_pos)...]
        for (i, r) in enumerate(rowvals(problem.lhs.mat[index, :]))
            problem.lhs.mat[index, r] = block_1[i]
        end
        problem.rhs.mat[index, index] = σ
        problem.rhs.mat[index, 2 * n_points + index] = ν

        nz = length(rowvals(problem.lhs.mat[n_points + index, :]))
        block_2[1:nz] .= [((1 + 5 * τ/3) * fd_θ_coefs)...,
                          hyp_θ_coefs...,
                          τ * fd_θ_coefs...]
        for (i, r) in enumerate(rowvals(problem.lhs.mat[n_points + index, :]))
            problem.lhs.mat[n_points + index, r] = block_2[i]
        end
        problem.rhs.mat[n_points + index, n_points + index] = one(Complex{T})


        nz = length(rowvals(problem.lhs.mat[2 * n_points + index, :]))
        block_3[1:nz] .= [ky * (∇T - 2 * ∇n/3)/B, hyp_θ_coefs...] 
        for (i, r) in enumerate(rowvals(problem.lhs.mat[2 * n_points + index, :]))
            problem.lhs.mat[2 * n_points + index, r] = block_3[i]
        end
        problem.rhs.mat[2 * n_points + index, 2 * n_points + index] = one(Complex{T})


        diag_pos = zeros(Complex{T}, error_order)
        index = n_points - j + 1
        diag_pos[error_order - j + 1] = one(Complex{T})
        fd_coeffs[:] .= fdm_coefs[1:error_order, error_order + 2 - j]
        hyp_coeffs[:] = hyp_diff_coefs[1:error_order, error_order + 2 - j]
        θ = θ_range[index]
        Bk_θ, Dk_θ = damping_drive_term(θ, kx, ky, geom)
        B = geom.B(θ)
        jac = geom.jac(θ)
        fd_θ_coefs[:] .= (-im/(Δθ*jac*B) * fd_coeffs)
        hyp_θ_coefs[:] .= (-im * hyp_z_coeff/(Δθ * jac * B)^problem.hyper_diffusion_order)*hyp_coeffs
        σ = (1.0 /(1.0 + Bk_θ * (1 + 5 * τ/3)))
        ν = -(Bk_θ * τ /(1.0 + Bk_θ * (1 + 5 * τ/3)))

        nz = length(rowvals(problem.lhs.mat[index, :]))
        block_1[1:nz] .= [(hyp_θ_coefs .+ (ky * ∇n/B + Dk_θ * (1 + 5 * τ/3)) .* diag_pos)...,
                          fd_θ_coefs...,
                          ((Dk_θ * τ) .* diag_pos)...]
        for (i, r) in enumerate(rowvals(problem.lhs.mat[index, :]))
            problem.lhs.mat[index, r] = block_1[i]
        end
        problem.rhs.mat[index, index] = σ
        problem.rhs.mat[index, 2 * n_points + index] = ν

        nz = length(rowvals(problem.lhs.mat[n_points + index, :]))
        block_2[1:nz] .= [((1 + 5 * τ/3) * fd_θ_coefs)...,
                          hyp_θ_coefs...,
                          τ * fd_θ_coefs...]
        for (i, r) in enumerate(rowvals(problem.lhs.mat[n_points + index, :]))
            problem.lhs.mat[n_points + index, r] = block_2[i]
        end
        problem.rhs.mat[n_points + index, n_points + index] = one(Complex{T})

        nz = length(rowvals(problem.lhs.mat[2 * n_points + index, :]))
        block_3[1:nz] .= [ky * (∇T - 2 * ∇n/3)/B, hyp_θ_coefs...] 
        for (i, r) in enumerate(rowvals(problem.lhs.mat[2 * n_points + index, :]))
            problem.lhs.mat[2 * n_points + index, r] = block_3[i]
        end
        problem.rhs.mat[2 * n_points + index, 2 * n_points + index] = one(Complex{T})


    end

    problem.lhs.mat .= problem.rhs.mat * problem.lhs.mat
    return nothing
end

## DEPRECATED ##
function dense_linear_operators(dw::Union{DriftWave{3, T, Fields}, DriftWaveBundle{3, T, E, Fields}},
                                geom::FieldlineGeometry{T},
                                problem::LinearProblem{3, S, T, E},
                                plasma::PlasmaParameters;
                               ) where {S, T, E, Fields}
  kx = dw.k_perp.kx
  ky = dw.k_perp.ky
  
  # For convenience extract the relevant plasma parameters
  ∇n = plasma.∇n
  ∇T = plasma.∇T
  τ = plasma.τ

  # Set the numerical hyperdiffusion parameters
  θ_range = range(start = dw.η_span.min, stop = dw.η_span.max, length = problem.n_points)
  Δθ = step(θ_range)
  n_points = problem.n_points
  rank = 3 * n_points
  lhs = zeros(Complex{T}, (rank, rank))
  rhs = Matrix{Complex{T}}(I, (rank, rank))

  hyper_diff_ε = problem.ε
  hyp_z_coeff = hyper_diff_ε*(im*0.5*Δθ)^problem.hyper_diffusion_order

  @inbounds for (index, θ) in enumerate(θ_range)
    Bk_θ, Dk_θ = damping_drive_term(θ, kx, ky, geom)
    B = geom.B(θ)
    jac = geom.jac(θ)

    # Compute the finite difference splines for the first order derivative operator
    # e.g. ∂/∂z → [c₁,c₂,…,cₙ₋₁,cₙ] where n = model.grid.derivative_error_order + 1
    fdm = parallel_derivative(index,n_points, 1, problem.derivative_error_order)
    # Compute the finite difference spline for the n-th order derivative operator
    # where n is specified by model.grid.hyper_diffusion_order
    hyper_diff = parallel_derivative(index, n_points,
                                     problem.hyper_diffusion_order,
                                     problem.derivative_error_order)

    # Deal with boundaries where the point beyond the range is assumed to be zero
    deriv_indices = findall(x -> ((x + index) > 0 && (x + index) <= n_points), fdm.grid)
    hyper_diff_indices = findall(x -> ((x + index) > 0 && (x + index) <= n_points), hyper_diff.grid)

    fdm_grid = fdm.grid[deriv_indices]
    # Apply the prefactor -i/√gB to each derivative term
    fdm_coefs = convert.(Complex{T}, fdm.coefs[deriv_indices])*(-im/(Δθ*jac*B))

    hyper_diff_grid = hyper_diff.grid[hyper_diff_indices]
    # Approximate the n-th order hyperdiffusion operator by taking only the
    # leading order term
    hyper_diff_coefs = hyper_diff.coefs[hyper_diff_indices] *
                       (-im * hyp_z_coeff/(Δθ * jac * B)^problem.hyper_diffusion_order)

    # Use array views to efficiently access the underlying data to apply the
    # finite differnce splines. For the three-field model with operator rank
    # 3N × 3N, the system can be broken into N × N blocks:
    #     || Φₖ ||    || A1 | A2 | A3 ||   || Φₖ ||
    # A ⋅ || Uₖ || ⇒  || A4 | A5 | A6 || ⋅ || Uₖ ||
    #     || Tₖ ||    || A7 | A8 | A9 ||   || Tₖ ||

    # Left-hand side operator
    # Block 1
    lhsView = view(lhs, index, index .+ hyper_diff_grid)
    lhsView .+= hyper_diff_coefs
    lhs[index, index] += ky * ∇n/B + Dk_θ * (1 + 5 * τ/3)

    # Block 2
    lhsView = view(lhs, index, (n_points+index) .+ fdm_grid)
    lhsView .+= fdm_coefs

    # Block 3
    lhs[index, 2 * n_points+index] += Dk_θ * τ

    # Block 4
    lhsView = view(lhs,n_points+index,index .+ fdm_grid)
    lhsView .+= fdm_coefs * (1 + 5 * τ/3)

    # Block 5
    lhsView = view(lhs, n_points + index, (n_points + index) .+ hyper_diff_grid)
    lhsView .+= hyper_diff_coefs

    # Block 6
    lhsView = view(lhs, n_points + index,(2 * n_points + index) .+ fdm_grid)
    lhsView .+= fdm_coefs * τ

    # Block 7
    lhs[2 * n_points + index, index] += ky * (∇T - 2 * ∇n/3)/B #+ 10*τ*Dk_θ/9
    #lhs[2*nPoints+index,2*nPoints+index] += 5*τ*Dk_θ/3

    # Block 8
    #
    # Block 9
    lhsView = view(lhs, 2 * n_points + index, (2 * n_points + index) .+ hyper_diff_grid)
    lhsView .+= hyper_diff_coefs

    # Right hand side operator
    # Block 1
    rhs[index, index] += Bk_θ * (1 + 5 * τ/3)

    #Block 3
    rhs[index, 2 * n_points + index] += Bk_θ * τ

  end
  return sparse(lhs), sparse(rhs)
end

