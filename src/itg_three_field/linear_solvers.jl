
"""
compute_linear_roots(kx::AbstractFloat,
ky::AbstractFloat,
pointRange::AbstractRange,
vec::Union{SplineType, Function},
plasma::PlasmaParameters,
geom::StructArray{FG},
problem::LinearProblem,
) where {FG <: AbstractFieldlineGeometry}

For a given eigenfunction specfied by `vec` and geometry interval, compute the unstable,
stable and marginally stable solutions, this can provide a check on the direct
eigenvalue calculation.
"""
function compute_linear_roots(k_perp::SpectralPoint{T},
                              bounds::NamedTuple{(:min, :max), Tuple{T, T}},
                              vec::Union{SplineType, Function},
                              geom::FieldlineGeometry{T},
                              plasma::PlasmaParameters{T},
                              problem::LinearProblem{3, S, T, E},
                             ) where {S <: AbstractSolverMethod, T, E}
    kx = k_perp.kx
    ky = k_perp.ky

    B = geom.B
    jac = geom.jac
    gxx = geom.g.xx
    gxy = geom.g.xy
    gyy = geom.g.yy
    Bx∇B_∇x = geom.Bx∇B.∇x
    Bx∇B_∇y = geom.Bx∇B.∇y


    # Gauss-Legendre quadrature with predefined knots and weights is
    # fast, easiest to do sequentially rather than parallel
    L3 = quadgl1D(L3_integrand, problem.quad_nodes, problem.quad_weights,
                  bounds, vec, B, jac,
                  z -> polarization_damping(z, kx, ky, gxx, gxy, gyy, B),
                  plasma; QuadType = T)

    L2 = quadgl1D(L2_integrand, problem.quad_nodes, problem.quad_weights,
                  bounds, ky, vec, B, jac,
                  z -> polarization_damping(z, kx, ky, gxx, gxy, gyy, B),
                  z -> curvature_drive(z, kx, ky, B, Bx∇B_∇x, Bx∇B_∇y),
                  plasma; QuadType = T)

    L1 = quadgl1D(L1_integrand, problem.quad_nodes, problem.quad_weights,
                  bounds, ky, vec, B, jac,
                  z -> polarization_damping(z, kx, ky, gxx, gxy, gyy, B),
                  z -> curvature_drive(z, kx, ky, B, Bx∇B_∇x, Bx∇B_∇y),
                  plasma; QuadType = T)

    L0 = quadgl1D(L0_integrand, problem.quad_nodes, problem.quad_weights,
                  bounds, ky, vec, B, jac, plasma; QuadType=T)

    # Use the Polynomials package to find the roots of the cubic equation
    # ω³ + (L₂/L₃)ω² + (L₁/L₃)ω + L₀/L₁ = 0
    roots = Polynomials.roots(Polynomial([L0/L3,L1/L3,L2/L3,1.0]))

    # Sort the modes, it may be more efficient to move this outside the function
    unstableMode = findfirst(x->imag(x)>0, roots)
    stableMode = findfirst(x->imag(x)<0, roots)
    marginal_mode = findfirst(x->isapprox(imag(x), zero(T)), roots)
    return SVector{3,Complex{T}}(roots[!isnothing(unstableMode) ? unstableMode : 1],
                                 roots[!isnothing(stableMode) ? stableMode : 2],
                                 roots[!isnothing(marginal_mode) ? marginal_mode : 3])
end

function marginal_mode(I::CartesianIndex,
                       model::FluidModel{3, T};
                      ) where {T}

    return marginal_mode(model.spectrum[I].ω[1], model.grid.k_perp[I], model.spectrum[I].η_span,
                         model.spectrum[I].Φ, model.geometry, model.plasma, model.problem)
end

"""
    marginal_mode(kx::AbstractFloat,
                  ky::AbstractFloat,
                  ω::Complex{T},
                  pointRange::AbstractRange,
                  vec::Union{SplineType, Function},
                  geom::StructArray{FG},
                  plasma::PlasmaParameters
                  problem::LinearProblem,
                 ) where {T, FG <: AbstractFieldlineGeometry}

For a given eigenfunction with unstable eigenvalue `ω` and inteprolated eigenmode `vec`
over the geometry interval `geom`, compute only the marginally stable solution,
which can also provide a check on `compute_linear_roots()`

# See also: [`compute_linear_roots`](@ref)
"""
function marginal_mode(ω::Complex{T},
                       k_perp::SpectralPoint{T},
                       bounds::NamedTuple{(:min, :max), Tuple{T, T}},
                       vec::Union{SplineType, Function},
                       geom::FieldlineGeometry{T},
                       plasma::PlasmaParameters{T},
                       problem::LinearProblem{3, S, T, E};
                      ) where {S, T, E}
    kx = k_perp.kx
    ky = k_perp.ky

    B = geom.B
    jac = geom.jac
    gxx = geom.g.xx
    gxy = geom.g.xy
    gyy = geom.g.yy
    Bx∇B_∇x = geom.Bx∇B.∇x
    Bx∇B_∇y = geom.Bx∇B.∇y

    L3 = quadgl1D(L3_integrand, problem.quad_nodes, problem.quad_weights,
                  bounds, vec, B, jac,
                  z -> polarization_damping(z, kx, ky, gxx, gxy, gyy, B),
                  plasma; QuadType=T)

    L2 = quadgl1D(L2_integrand, problem.quad_nodes, problem.quad_weights,
                  bounds, ky, vec, B, jac,
                  z -> polarization_damping(z, kx, ky, gxx, gxy, gyy, B),
                  z -> curvature_drive(z, kx, ky, B, Bx∇B_∇x, Bx∇B_∇y),
                  plasma; QuadType =T)
    
    kperp_2 = quadgl1D(kperp2_integrand, problem.quad_nodes, problem.quad_weights,
                       bounds, kx, ky, vec, B, jac, gxx, gxy, gyy; QuadType = T) 
    kparallel_2 = quadgl1D(kparallel2_integrand, problem.quad_nodes, problem.quad_weights,
                           bounds, vec, B, jac; QuadType = T)
    return -L2/L3 - 2*real(ω), kperp_2, kparallel_2
end

function L3_integrand(x::S,
                      ϕ::Union{SplineType, Function},
                      B::Union{SplineType, Function},
                      sqrtg::Union{SplineType, Function},
                      Bk::Union{SplineType, Function},
                      plasma::PlasmaParameters{T},
                     ) where {S, T}
    return sqrtg(x)*B(x)*(1+Bk(x)*(1+5/3*plasma.τ))*abs2(ϕ(x))
end

function L2_integrand(x::S,
                     ky::T,
                     ϕ::Union{SplineType, Function},
                     B::Union{SplineType, Function},
                     sqrtg::Union{SplineType, Function},
                     Bk::Union{SplineType, Function},
                     Dk::Union{SplineType, Function},
                     plasma::PlasmaParameters{T},
                     ) where {S, T}
    return abs2(ϕ(x))*sqrtg(x)*B(x)*(ky*
           ((plasma.∇T-2/3*plasma.∇n)*plasma.τ*Bk(x) - plasma.∇n)/B(x)
           -Dk(x)*(1+5/3*plasma.τ))
end

function L1_integrand(x::S,
                     ky::T,
                     ϕ::Union{SplineType, Function},
                     B::Union{SplineType, Function},
                     sqrtg::Union{SplineType, Function},
                     Bk::Union{SplineType, Function},
                     Dk::Union{SplineType, Function},
                     plasma::PlasmaParameters{T},
                    ) where {S, T}
    return -sqrtg(x)*B(x)*(ky*Dk(x)*(plasma.∇T-2/3*plasma.∇n)/B(x)*plasma.τ)*abs2(ϕ(x)) - (1+5/3*plasma.τ)*abs2(Interpolations.gradient(ϕ,x)[])/(sqrtg(x)*B(x))
end

function L0_integrand(x::S,
                      ky::T,
                      ϕ::Union{SplineType, Function},
                      B::Union{SplineType, Function},
                      sqrtg::Union{SplineType, Function},
                      plasma::PlasmaParameters{T},
                     ) where {S, T}
    return -ky*(plasma.∇T-2/3*plasma.∇n)/B(x)*plasma.τ*abs2(Interpolations.gradient(ϕ,x)[])/(sqrtg(x)*B(x))
end

function kparallel2(I::CartesianIndex,
                    model::FluidModel{3, T};
                   ) where {T}
    return kparallel2(model.spectrum[I].η_span, model.spectrum[I].Φ,
                      model.geometry, model.problem)
end

"""
    kparallel2(pointRange::AbstractRange,
               vec::Union{SplineType, Function},
               geom::StructArray{FG},
               problem::LinearProblem;
               quadT::Type=Float64
              ) where {FG <: AbstractFieldlineGeometry}

For a given eigenfunction specfied by `vec` and geometry interval, compute the
effective paralle wavenumber squared: ⟨k∥²⟩
"""
function kparallel2(bounds::NamedTuple{(:min, :max), Tuple{T, T}},
                    vec::Union{SplineType, Function},
                    geom::FieldlineGeometry{T},
                    problem::LinearProblem{3, S, T, E};
                   ) where {S, T, E}
    return kparallel2(bounds, vec, geom.B, geom.jac, problem)
end

function kparallel2(bounds::NamedTuple{(:min, :max), Tuple{T, T}},
                    vec::Union{SplineType, Function},
                    B::Union{SplineType, Function},
                    sqrtg::Union{SplineType, Function},
                    problem::LinearProblem{3, S, T, E};
                   ) where {S, T, E}
    ϕ_norm = quadgl1D(fieldline_norm, problem.quad_nodes, problem.quad_weights,
                      bounds, vec, B, sqrtg; QuadType=T)
    dϕ_avg = quadgl1D(kparallel2_integrand, problem.quad_nodes, problem.quad_weights,
                      bounds, vec, B, sqrtg; QuadType=T)
    return dϕ_avg/ϕ_norm
end

function fieldline_norm(x::T,
                        f::Union{SplineType, Function},
                        B::Union{SplineType, Function},
                        sqrtg::Union{SplineType, Function},
                       ) where {T}
    return sqrtg(x)*B(x)*abs2(f(x))
end

function kparallel2_integrand(x::T,
                              ϕ::Union{SplineType, Function},
                              B::Union{SplineType, Function},
                              sqrtg::Union{SplineType, Function};
                             ) where {T}
    return abs2(Interpolations.gradient(ϕ,x)[])/(sqrtg(x)*B(x))
end

function kperp2_integrand(x::T,
                          kx::S,
                          ky::S,
                          ϕ::Union{SplineType, Function},
                          B::Union{SplineType, Function},
                          sqrtg::Union{SplineType, Function},
                          gxx::Union{SplineType, Function},
                          gxy::Union{SplineType, Function},
                          gyy::Union{SplineType, Function};
                         ) where {S, T}
    return sqrtg(x)*B(x)*(kx^2*gxx(x) + 2*kx*ky*gxy(x) + ky^2*gyy(x))*abs2(ϕ(x))
end
"""
    sort_modes(kx,ky,target,evals,evecs,geom,model)

Sorts the modes given by `evals` and `evecs` according to a combintation of growth rate,
k∥² and possibly phase
"""
function sort_modes(dw::Union{DriftWave{3, T, Fields}},
                    geom::FieldlineGeometry{T},
                    problem::LinearProblem{S, T, E};
                    weights::Vector{T}=[0.5,0.25,0.25],
                   ) where {S, T, E, Fields}


  gammas = imag(evals)./maximum(imag(evals))
  kparallel = zeros(T,nev)
  phase_score = zeros(T,nev)
  field_labels = fields(dw)

  for ev_index in eachindex(dw)
    # For the three-field model ϕ will always span the first N points
    ϕ = dw.modes[ev_index][field_labels[1]]
    kparallel[ev_index] = kparallel2(mode.η_span, ϕ, geom.B, geom.jac, problem)
    phase = cross_phase(mode, field_labels[1], field_labels[3], problem.n_points)
    phase_score[ev_index] = sign(phase)*(1.0 - abs(0.5π - abs(phase))/(0.5π))
  end
  kparallel .= 1.0 ./ (kparallel ./ minimum(kparallel))

  mode_score = (weights[1] .* gammas .+  weights[2] .* kparallel
               .+ weights[3] .* phase_score)/sum(weights)
  return sortperm(mode_score)
end
