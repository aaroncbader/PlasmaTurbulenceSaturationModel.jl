struct direct <: AbstractSolverMethod end

#=
Methods specfic to Jacobi-Davidson
=#
struct JDQZ{T} <: AbstractSolverMethod
    solver::JacobiDavidson.CorrectionSolver
    max_iter::Int
    subspace_dimensions::StepRangeLen
    tolerance::T
end

function JDQZ(rank::Int;
              solver = JacobiDavidson.GMRES,
              correction_iterations = 7,
              max_iter = 10000,
              subspace_dimensions = 50:200,
              tolerance = sqrt(eps()),
             )
    return JQDZ{typeof(tolerance)}(solver(rank, correction_iterations), max_iter, subspace_dimensions, tolerance)
end


struct ArnoldiMethod{T} <: AbstractSolverMethod
    krylovdim::Int 
    maxiter::Int
    tol::T
end

function ArnoldiMethod(;
                 krylovdim = 30,
                 maxiter = 100,
                 tolerance = sqrt(eps()),
                )
    return ArnoldiMethod{typeof(tolerance)}(krylovdim, maxiter, tolerance)
end


function compute_eigenmodes(I::CartesianIndex,
                            model::FluidModel{N, T};
                            kwargs...
                           ) where{N, T}
    #lhs, rhs = sparse.(dense_linear_operators(model.spectrum[I], model.geometry,
    #                                          model.problem, model.plasma))
    build_linear_operators!(model.problem, model.spectrum[I], model.geometry, model.plasma)
    return compute_eigenmodes(model.problem; kwargs...)
end


function compute_eigenmodes(problem::LinearProblem{N, S, T, E};
                            target::Complex{T} = one(T) * im,
                            kwargs...
                           ) where {N, S <: JDQZ, T, E}
    pschur, _ = jdqz(problem.lhs.mat, problem.rhs.mat,
                     solver= problem.solver.solver,
                     target= Near(real(target)+10im*imag(target)),
                     pairs= E,
                     subspace_dimensions = problem.solver.subspace_dimensions,
                     max_iter = problem.solver.max_iter,
                     tolerance = problem.solver.tolerance,
                     numT = Complex{T},
                     verbosity=1);

    Q = pschur.Q.basis
    Z = pschur.Z.basis
    evals, evecs = eigen(Z'*lhs*Q, Z'*rhs*Q)
    return evals, Q*evecs
end

function compute_eigenmodes(problem::LinearProblem{N, S, T, E};
                            init_vec::Vector{Complex{T}} = rand(Complex{T}, size(problem.lhs.mat, 1)),
                            kwargs...
                           ) where {N, S <: ArnoldiMethod, T, E}
    evals = T(NaN)
    evecs = nothing
    precond_vec = copy(init_vec)
    while isnan(first(evals))
        evals, evecs, _ = try KrylovKit.eigsolve(problem.lhs.mat, precond_vec, E, :LI;
                                            tol = problem.solver.tol,
                                            krylovdim = problem.solver.krylovdim,
                                            maxiter = problem.solver.maxiter)
        catch
            precond_vec = rand(Complex{T}, size(problem.lhs.mat, 1))
            T(NaN), nothing, nothing
        end
    end
    evec_matrix = Matrix{Complex{T}}(undef, N * problem.n_points, length(evecs))
    for i in axes(evecs, 1)
        evec_matrix[:, i] = evecs[i][:]
    end
    return map(e -> trunc(real(e), digits = 4) + trunc(imag(e), digits = 4)*im, evals), evec_matrix
end

#=
function computeConditionNumber(kx::Float64,ky::Float64,geom::FieldlineGeometry;errorOrder=2)
  A, B = denseLinearOperators(kx,ky,geom,errorOrder=errorOrder)
  return cond(inv(B)*A)
end

function computeConditionNumber(kx::AbstractRange,ky::AbstractRange,geom::FieldlineGeometry;errorOrder=2)
  conditionNumbers = Array{Float64}(undef,length(kx),length(ky))
  for (iy,iky) in enumerate(ky)
    for (ix,ikx) in enumerate(kx)
      conditionNumbers[ix,iy] = computeConditionNumber(ikx,iky,geom,errorOrder=errorOrder)
    end
  end
  return conditionNumbers
end

=#
#function initialValueSolver(kx::Float64,ky::Float64,geom::FieldlineGeometry)
#
#end

