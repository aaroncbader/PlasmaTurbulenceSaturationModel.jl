# Set the finite difference grid with the zero boundary condition
"""
    parallel_derivative(rowIndex::Int,maxIndex::int,diffOrder::Int,errorOrder::Int)

Compute the finite difference spline coefficients assuming a zero boundary
condition for the row beyond the first and last row
"""
function parallel_derivative(rowIndex::Int,
                             maxIndex::Int,
                             diffOrder::Int,
                             errorOrder::Int,
                            )
    splineBound = div(errorOrder,2)
    lbound = -splineBound
    ubound = splineBound
    if rowIndex <= splineBound
        lbound = -splineBound + abs(splineBound-rowIndex)
        ubound = errorOrder-rowIndex
    elseif rowIndex > (maxIndex - splineBound)
        lbound = maxIndex - errorOrder - rowIndex + 1
        ubound = maxIndex - rowIndex + 1
    end
    return FiniteDifferenceMethod(lbound:ubound,diffOrder)
end

"""
    function quadgl1D(f,
                      nodes::AbstractVector{T},
                      weights::AbstractVector{T},
                      a::T,
                      b::T,
                      args...;
                      quadT::Type=Any,
                     ) where T

Perform 1D Gauss-Laguerre quadrature of the function `f` between [a,b]
with the nodes and weights defined by a Gaussian quadrature scheme.
The returned type can adjusted with the `quadT` keyword.
"""
function quadgl1D(f,
                  nodes::AbstractVector{T},
                  weights::AbstractVector{T},
                  bounds::NamedTuple{(:min, :max), Tuple{T, T}},
                  args...;
                  QuadType::Type=Any,
                 ) where T
  a, b = bounds
  #res = Vector{quadT === Any ? eltype(nodes) : quadT}(undef,length(nodes))
  res = zero(QuadType === Any ? eltype(nodes) : QuadType)
  #map!((x,w)->w*f(0.5*(a+b+(b-a)*x),args...),res,nodes,weights)
  @inbounds @simd for i ∈ eachindex(nodes)
    res += weights[i]*f(0.5*(a+b+(b-a)*nodes[i]),args...)
  end
  #return 0.5*(b-a)*sum(res)
  return 0.5*(b-a)*res
end

"""
    vector2range(v)

Convert an evenly spaced vector points to a range
"""
function vector2range(v::AbstractVector{T};
                      convert_type::Type = T,
                     ) where {T}
    if typeof(v) <: AbstractRange
        return v
    else
        v_diff = @view(v[2:end]) .- @view(v[1:end-1])
        v_diff_mean = sum(v_diff)/length(v_diff)
        all(v_diff .≈ v_diff_mean) || throw(error("Step between vector elements must be consistent"))
        return range(convert(T, v[1]), step = convert(T, v_diff_mean), length = length(v))
    end
end

"""
    gaussian_mode(x,σ=1,μ=0.0)

Function for a normalized Gaussian distribution: 1/√(2πσ²)*exp(-(x-μ)²/2σ²)
"""
function gaussian_mode(x, σ=1, μ=0.0)
    return exp(-0.5 * ((x - μ) / σ)^2)/sqrt(2 * π * σ^2)
end

"""
    cross_phase(vec::AbstractVector,nFields::Integer,X::Integer,Y::Integer)

Compute the average cross phase between two different fields given
by contiguous subvectors of `vec` denoted by `X` and `Y`.
For a vector representing the solution to a linear `nFields`-dimensional
system discretized by `M` points, `vec` will have length `M × nFields`
and the field given by `X::Integer` will span `[M × (X-1)+1:M × X]`.
"""
function cross_phase(mode::DriftWave{N, T, Fields},
                     field_1::Union{Int, Symbol},
                     field_2::Union{Int, Symbol},
                     n_points::Int;
                     n_bins::Int = 100,
                    ) where {N, T, Fields}
    ϕ = Vector{Complex{T}}(undef, n_points)
    ψ = similar(ϕ)
    η_range = range(start = mode.η_span.min,
                    stop = mode.η_span.max, length = n_points)
    @inbounds for (i, η) in enumerate(η_range)
        ϕ[i] = getproperty(mode, field_1)(η)
        ψ[i] = getproperty(mode, field_2)(η)
    end

    bins = -π:2π/(n_bins - 1):π
    bin_step = step(bins)
    n_bins2 = div(n_bins, 2)
    weighted_phase_counts = zeros(T, n_bins)

    @inbounds for i = 1:n
        ν = ϕ[i] * conj(ψ[i])
        α = atan(imag(ν), real(ν))
        β = abs(ν)
        
        # Find the binindex
        bindex = floor(Int, α / bin_step) + n_bins2 + 1
        weighted_phase_counts[bindex] += β
    end
    return sum(weighted_phase_counts.*bins)./sum(weighted_phase_counts)
end

"""
    vec_norm(points,vec,geom,problem;quadT=Float64)

Compute the norm of a vector over the field line

# Arguments:
- `points::AbstractRange` : Range of points
- `vec::Interpolation.Extrapolation` : Interpolated vector of the range of `points`
- `geom::StructArray{AbstractFieldlineGeometry}` : Geometry of the field line over the range of `points`
- `problem::LinearProblem` : `LinearProblem` containing the quadrature nodes and weights
"""
function vec_norm(bounds::NamedTuple{(:min, :max), Tuple{T, T}},
                  vec::Union{SplineType, Function},
                  geom::FieldlineGeometry{T},
                  problem::LinearProblem{N, S, T, E};
                  QuadType::Type=Float64,
                 ) where {N, S, T, E}
  return quadgl1D(vec_norm_integrand, problem.quad_nodes, problem.quad_weights,
                  bounds, vec, geom.B, geom.jac, QuadType=QuadType)
end

function vec_norm_integrand(x,
                            vec::Union{SplineType, Function},
                            modB::Union{SplineType, Function},
                            sqrtg::Union{SplineType, Function};
                           )
  return sqrtg(x)*modB(x)*abs2(vec(x))
end

