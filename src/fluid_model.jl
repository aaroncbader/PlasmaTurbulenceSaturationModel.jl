
"""
    FluidModel{FT, NF, NV, FG}

Composite type containing the fields required for the saturation calculation.
The `FluidModel` type is parametered by the floating point type `FT`,
the number of independent fields `NF`, the number of independent eigenvectors
`NV` and the fieldline geometry type `FG <: AbstractFieldlineGeometry`.

# Summary
struct FluidModel <: Any

# Fields
- `plasma::PlasmaParametes` : Plasma parameters
- `spectralGrid::SpectralGrid` : Spectral grid parameters
- `problem::LinearProblem` : Linear problem specification
- `geometry::StructArray{FG}` : Array containing the geometry compoents along the fieldline
- `geomParameters::AbstractGeometryParameters`: Structure with the global geometry parameters
- `indices::Matrix{AbstractRange}` : Matrix of unit ranges holding the indices spanned by each eigenmode for a given `problem.solnInterval`
- `points::Matrix{AbstractRange}` : Matrix of ranges representing the angle in radians spanned by each eigenmode for a given `problem.solnInterval`
- `eigenvalues::Matrix{SVector{NF,Complex{FT}}}` : Matrix holding the eigenvalue solutions to the linear eigenvalue problem
- `eigenvectors::Matrix{SVector{NV,Interpolations.Extapolation}}` : Matrix holding the `NF` independent interpolated eigenvector solutions to the linear eigenvalue problem
- `vecNorms::Matrix{NV,FT}` : Matrix holding the norm of each solution in `eigenvectors`
- `coupling_matrix::Matrix{SMatrix{NF,NF,Complex{FT}}}`
- `inv_coupling_matrix::Matrix{SMatrix{NF,NF,Complex{FT}}}`
- `instability::BitArray` : Matrix indicting whether an unstable linear solution exists for each pair of indices

# See also: [`PlasmaParameters`](@ref), [`SpectralGrid`](@ref), [`SolverMethod`](@ref), [`LinearProblem`](@ref)
"""
# The fluid model should hold the results of the linear mode calculations
# that can then be used to calculate the couplings
struct FluidModel{N, T}
    plasma::PlasmaParameters
    grid::SpectralGrid
    problem::LinearProblem
    geometry::FieldlineGeometry
    spectrum::Array{DriftWaveBundle, 2}
end

function FluidModel(plasma::PlasmaParameters,
                    grid::SpectralGrid,
                    prob::LinearProblem,
                    geom::FieldlineGeometry;
                    n_fields::Int = 3,
                    n_ev::Int = 1,
                    field_labels = (:Φ, :U, :T),
                    FloatType::Type=Float64,
                   )
    N = n_fields
    E = n_ev
    Fields = field_labels
    T = FloatType
    spectrum = Array{DriftWaveBundle{N, T, E, Fields}, 2}(undef, size(grid.k_perp))
    for I in CartesianIndices(spectrum)
        kx = grid.k_perp[I].kx
        ky = grid.k_perp[I].ky
        ηₖ = -kx/(geom.parameters.s_hat * ky)
        η_min = ηₖ - 0.5*prob.soln_interval
        η_max = ηₖ + 0.5*prob.soln_interval
        amp = convert(T, power_spectrum_coeff(I, grid))
        spectrum[I] = DriftWaveBundle(I, grid.k_perp.kx[I], grid.k_perp.ky[I],
                                      amp, η_min, η_max, prob.n_points;
                                      n_fields = N, n_ev = E, FloatType = T,
                                      field_labels = Fields, 
                                     )
    end

    return FluidModel{n_fields, FloatType}(plasma, grid, prob, geom, spectrum)
end

"""
    nKxModes(model::FluidModel)

Retrieve the number of kx modes from the `grid` member of `model`
"""
function nKxModes(model::FluidModel)
  return nKxModes(getfield(model,:spectralGrid))
end

"""
    nKyModes(model::FluidModel)

Retrieve the number of ky modes from the `grid` member of `model`
"""
function nKyModes(model::FluidModel)
  return nKyModes(getfield(model,:spectralGrid))
end

"""
    Δkx(model::FluidModel)

Retrive the kx grid spacing from the `grid` member of `model`
"""
function Δkx(model::FluidModel)
  return Δkx(getfield(model,:spectralGrid))
end

"""
    Δky(model::FluidModel)

Retrieve the ky grid spacing from the `grid` member of `model`
"""
function Δky(model::FluidModel)
  return Δky(getfield(model,:spectralGrid))
end

"""
    nfields(model::FluidModel)

Retrieve the number of dynamical fields from `model`
"""
function n_fields(model::FluidModel{N, T}) where {T, N}
    return N
end

function kx(model::FluidModel;
            negativeModes=false,
           )
  return !negativeModes ? model.spectralGrid.kxRange : -last(model.spectralGrid.kxRange):Δkx(model):last(model.spectralGrid.kxRange)
end

function ky(model::FluidModel;
           )
  return model.spectralGrid.kyRange
end

function eigenvalues(model::FluidModel,
                     kxIndex::Integer,
                     kyIndex::Integer,
                     modeIndex::Integer=0;
                    )
  return modeIndex == 0 ? model.eigenvalues[kxIndex,kyIndex] : model.eigenvalues[kxIndex,kyIndex][modeIndex]
end

function eigenvector(model::FluidModel,
                     kxIndex::Integer,
                     kyIndex::Integer,
                     vectorIndex::Integer=1;
                     array::Bool=false,
                    )
  if !array
    return model.eigenvectors[kxIndex,kyIndex]
  else
    evec = model.eigenvectors[kxIndex,kyIndex]
    return evec.itp.itp.coefs[evec.itp.itp.parentaxes...]
  end
end

function points(model::FluidModel,
                kxIndex::Integer,
                kyIndex::Integer;
               )
  return model.points[kxIndex,kyIndex]
end

function indices(model::FluidModel,
                 kxIndex::Integer,
                 kyIndex::Integer;
                )
  return model.indices[kxIndex,kyIndex]
end
