
struct SpectralPoint{T}
    kx::T
    ky::T
end

"""
    SpectralGrid

Composite type holding the spectral grid properties
"""
struct SpectralGrid{T}
    Δk::NamedTuple
    k_perp::StructArray
    kx_map::NamedTuple
    power_spectrum::NamedTuple
end

"""
    SpectralGrid(; kwargs...)

Construct a `SpectralGrid` type from keyword parameters that holds the spectral
discretization parameters for computing the couplings and saturation targets

# Arguments
- `nkx::Integer`=21 : Number of kx modes (including kx = 0 mode)
- `nky::Integer`=20 : Number of ky modes (excluding ky = 0 mode)
- `Δkx::AbstractFloat`=0.05 : Spacing between kx modes in units of 1/ρₛ
- `Δky::AbstractFloat`=0.05 : Spacing between ky modes in units of 1/ρₛ
- `kxStart::AbstractFloat`=0.0 ; Initial kx
- `kyStart::AbstractFloat`=0.0 ; Initial ky, specifying 0.0 will default to `Δky`
- `kxSpecLimit::AbstractFloat`=0.2 : kx wavenumber where energy spectrum spectral break occurs
- `kySpecLimit::AbstractFloat`=0.6 : ky wavenumber where energy spectrum spectral break occurs
- `kxSpecPower::AbstractFloat`=2.0 : Exponent of energy spectrum in the kx direction after the spectral break
- `kySpecPower::AbstractFloat`=4/3 : Exponent of energy spectrum in the ky direction after the spectral break
"""
function SpectralGrid(;
                      nkx=21,
                      nky=20,
                      Δkx = 0.05,
                      Δky = 0.05,
                      kxSpecLimit = 0.2,
                      kySpecLimit = 0.6,
                      kxSpecPower = 2.0,
                      kySpecPower = 4/3,
                      FloatType::Type = Float64,
                     )
    # Fluid model is invalid for k > 1ρₛ
    δkx, δky = convert.(FloatType, promote(Δkx, Δky))
    T = typeof(δkx)
    nkx = min(nkx, floor(typeof(nkx), one(T) / δkx) + 1)
    nky = min(nky, floor(typeof(nky), one(T) / δky))

    k_perp = Array{SpectralPoint{T}, 2}(undef, 2 * nkx - 1, nky)
    kx_map = circshift(-(nkx-1):(nkx-1), -(nkx-1))
    reverse_map = 1:(2 * nkx - 1)
    for I in CartesianIndices(k_perp)
        ikx = I[1] <= nkx ? I[1] - 1 : -2 * nkx + I[1]
        k_perp[I] = SpectralPoint{T}(δkx * ikx, δky * I[2])
    end

    return SpectralGrid{T}((kx = δkx,  ky = δky),
                           StructArray(k_perp),
                           (forward = kx_map, reverse = FFTView(reverse_map)),
                           (kx_limit = kxSpecLimit, kx_exp = kxSpecPower,
                            ky_limit = kySpecLimit, ky_exp = kySpecPower))
end


function Base.size(grid::SpectralGrid{T}) where {T}
  return size(grid.k_perp)
end

function Base.size(grid::SpectralGrid{T},
                   dim::Int;
                  ) where {T}
  return size(grid.k_perp, dim)
end

function Base.length(grid::SpectralGrid{T}) where {T}
    return length(grid.k_perp)
end

"""
    n_kx_modes(grid::SpectralGrid)

Return the number of kx modes associated with `grid`
"""
function n_kx_modes(grid::SpectralGrid{T};
                    neg_kx::Bool = true,
                   ) where {T}
    return neg_kx ? size(grid, 1) : div(size(grid, 1) + 1, 2)
end

"""
    n_kx_modes(grid::SpectralGrid)

Return the number of ky modes associated with `grid`
"""
function n_ky_modes(grid::SpectralGrid)
    return size(grid, 2)
end

"""
    Δkx(grid::SpectralGrid)

Return the kx grid spacing associated with `grid`
"""
function Δkx(grid::SpectralGrid{T}) where {T}
    return grid.Δk.kx
end

"""
    Δky(grid::SpectralGrid)

Return the ky grid spacing associated with `grid`
"""
function Δky(grid::SpectralGrid{T}) where {T}
    return grid.Δk.ky
end

function triplet_matching(K1::Union{CartesianIndex, NTuple{2, Int}},
                          K2::Union{CartesianIndex, NTuple{2, Int}},
                          kx_map::NamedTuple)
    K3_x = kx_map.forward[K1[1]] - kx_map.forward[K2[1]]
    return (minimum(kx_map.forward) <= K3_x <= maximum(kx_map.forward) ?
            CartesianIndex(kx_map.reverse[K3_x], abs(K1[2] - K2[2])) :
            nothing)
end

function power_spectrum_coeff(I::CartesianIndex,
                              grid::SpectralGrid;
                             )
    kx = grid.k_perp.kx[I]
    ky = grid.k_perp.ky[I]
    return ((abs(kx) < grid.power_spectrum.kx_limit ? 1.0 : (grid.power_spectrum.kx_limit/abs(kx))^(grid.power_spectrum.kx_exp)) *
            (ky < grid.power_spectrum.ky_limit ? 1.0 : (grid.power_spectrum.ky_limit / ky) ^ grid.power_spectrum.ky_exp))
end
