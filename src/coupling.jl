
"""
    overlap_interval(range1::BoundsTuple, range2::BoundsTuple)
    overlap_interval(range1::BoundsTuble, range2::BoundsTuple, range3::BoundsTuple)

Compute the interval over which two (or three) eigenfunctions overlap,
returning `nothing` if no overlapping interval exists
"""
function overlap_interval(range1::BoundsTuple{T},
                          range2::BoundsTuple{T};
                         ) where {T}
    if range1.min >= range2.max
      return nothing
    elseif range1.max <= range2.min
      return nothing
    else
      return (min = max(range1.min, range2.min) + sqrt(eps(T)),
              max = min(range1.max, range2.max) - sqrt(eps(T)))
    end
end

function overlap_interval(range1::BoundsTuple{T},
                          range2::BoundsTuple{T},
                          range3::BoundsTuple{T};
                         ) where {T}
  overlap_points_12 = overlap_interval(range1, range2)
  overlap_points_23 = overlap_interval(range2, range3)
  return (!isnothing(overlap_points_12) && !isnothing(overlap_points_23)) ?
          overlap_interval(overlap_points_12, overlap_points_23) : nothing
end


"""
    overlap_integrand(x, u, v, sqrtg, modB)
    overlap_integrand(x, u, v, w, sqrtg, modB)

Compute the mode overlap integrand at point `x` between 
modes `u` and `v` or `u`, `v`, and `w`.
"""
function overlap_integrand(x::AbstractFloat,
                           u::Union{SplineType, Function},
                           v::Union{SplineType, Function},
                           sqrtg::Union{SplineType, Function},
                           modB::Union{SplineType, Function};
                          )
  return sqrtg(x)*modB(x)*conj(u(x))*v(x)
end

function overlap_integrand(x::AbstractFloat,
                           u::Union{SplineType, Function},
                           v::Union{SplineType, Function},
                           w::Union{SplineType, Function},
                           sqrtg::Union{SplineType, Function},
                           modB::Union{SplineType, Function};
                          )
  return sqrtg(x)*modB(x)*conj(u(x))*v(x)*w(x)
end

"""
    overlap_integral(mode_1::DriftWave, mode_2::DriftWave, geom::FieldlineGeometry, problem::LinearProblem, field::Union{Int, Symbol})
    overlap_integral(mode_1::DriftWave, mode_2::DriftWave, mode_3::DriftWave, geom::FieldlineGeometry, problem::LinearProblem, field::Union{Int, Symbol})

Compute the overlap between eigenmode field `field` between the modes `mode_1` and `mode_2`
or modes `mode_1`, `mode_2` and `mode_3`.
"""
function overlap_integral(mode_1::DriftWave{N, T, Fields},
                          mode_2::DriftWave{N, T, Fields},
                          mode_3::DriftWave{N, T, Fields},
                          geom::FieldlineGeometry{T},
                          problem::LinearProblem{N, S, T, E};
                          QuadType::Type = T,
                          field::Union{Int, Symbol} = 1,
                        ) where {N, S, T, E, Fields}
  overlap_points = overlap_interval(mode_1.η_span, mode_2.η_span, mode_3.η_span)

  if !isnothing(overlap_points)
    return (overlap_integral(overlap_points, mode_1.β[field], mode_2.β[field],
                             mode_3.β[field], geom, problem; QuadType=QuadType)/((mode_1.norms*mode_2.norms*mode_3.norms)^(1/3)),
            overlap_points)
  else
    return zero(Complex{T}), nothing
  end
end

"""
    overlap_integral(overlap_points::BoundsTuple{T}, u, v, geom::FieldlineGeometry, problem::LinearProblem; QuadType::Type = T)
    overlap_integral(overlap_points::BoundsTuple{T}, u, v, w, geom::FieldlineGeometry, problem::LinearProblem; QuadType::Type = T)

Compute the overlap between the functions `u` and `v` or
`u`, `v`, and `w` using 1D Gauss-Legendre quadrature.
"""
function overlap_integral(overlap_points::BoundsTuple{T},
                          u::Union{SplineType, Function},
                          v::Union{SplineType, Function},
                          w::Union{SplineType, Function},
                          geom::FieldlineGeometry{T},
                          problem::LinearProblem{N, S, T, E};
                          QuadType::Type=T
                         ) where {N, S, T, E}
  return quadgl1D(overlap_integrand, problem.quad_nodes, problem.quad_weights,
                  overlap_points, u, v, w, geom.jac, geom.B, QuadType=QuadType)
end


function overlap_integral(mode_1::DriftWave{N, T, Fields},
                          mode_2::DriftWave{N, T, Fields},
                          geom::FieldlineGeometry{T},
                          problem::LinearProblem{N, S, T, E};
                          QuadType::Type = T,
                          field::Union{Int, Symbol} = 1,
                         ) where {N, S, T, E, Fields}
  overlap_points = overlap_interval(mode_1.η_span, mode_2.η_span)

  if !isnothing(overlap_points)
    return (overlap_integral(overlap_points, mode_1.β[field], mode_2.β[field],
                             geom, problem; QuadType = QuadType)/(sqrt(mode_1.norms)*sqrt(mode_2.norms)),
            overlap_points)
  else
    return zero(Complex{T}), nothing
  end
end

function overlap_integral(overlap_points::BoundsTuple{T},
                          u::Union{SplineType, Function},
                          v::Union{SplineType, Function},
                          geom::FieldlineGeometry{T},
                          problem::LinearProblem{N, S, T, E};
                          QuadType::Type=T
                         ) where {N, S, T, E}
  return quadgl1D(overlap_integrand, problem.quad_nodes, problem.quad_weights,
                  overlap_points, u, v, geom.jac, geom.B, QuadType=QuadType)
end

"""
    CouplingData{T}

# Fields
- `τ::Complex{T}`: Complex three wave interaction time
- `C::Complex{T}`: Complex coupling coefficient
- `overlap::Compelx{T}`: Value of the overlap integral
- `value::T`: Value of the metric
- `triplet::NTuple{3, CartesianIndex}
"""
struct CouplingData{T}
    τ::Complex{T}
    C::Complex{T}
    overlap::Complex{T}
    value::T
    triplet::NTuple{3, Tuple{Int8, Int8}}
    which_ev::NTuple{3, Int8}
    which_roots::NTuple{3, Int8}
end

Base.show(io::IO, x::CouplingData{T}) where {T} =
    print(io, "CouplingData{$T}: k = (", x.triplet[1][1],", ",x.triplet[1][2],")[",x.which_ev[1],"].",x.which_roots[1],
              ", k' = (",x.triplet[2][1],", ",x.triplet[2][2],")[",x.which_ev[2],"].",x.which_roots[2],
              ", k'' = (",x.triplet[3][1],", ",x.triplet[3][2],")[",x.which_ev[3],"].",x.which_roots[3],"\n",
              "                       τ = ",x.τ,"\n",
              "                       C = ",x.C,"\n",
              "                       overlap = ",x.overlap)

function CouplingData(triplet::NTuple{3, CartesianIndex};
                      kwargs...
                     )
    return CouplingData((Int8.(Tuple(triplet[1])), Int8.(Tuple(triplet[2])),
                         Int8.(Tuple(triplet[3]))); kwargs...)
end

function CouplingData(triplet::NTuple{3, Tuple{Int8, Int8}};
                      FloatType::Type = Float64,
                     )
    zero_int8 = zero(Int8)
    return CouplingData{FloatType}(zero(Complex{FloatType}),
                                   zero(Complex{FloatType}),
                                   zero(Complex{FloatType}),
                                   zero(FloatType),
                                   triplet,
                                   (zero_int8, zero_int8, zero_int8),
                                   (zero_int8, zero_int8, zero_int8))
end

function CouplingData(I::CartesianIndex,
                      J::CartesianIndex,
                      K::CartesianIndex;
                      FloatType::Type = Float64,
                     )
    return CouplingData((Int8.(Tuple(I)), Int8.(Tuple(J)), Int8.(Tuple(K))); FloatType = FloatType)
end
            
