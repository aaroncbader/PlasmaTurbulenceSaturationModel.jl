

"""
    LinearProblem{N, S <: AbstractSolverMethod, T, E}

Composite type holding the properties for solving the linear drift wave problem
"""
struct LinearProblem{N, S <: AbstractSolverMethod, T, E}
    solver::S
    n_points::Int
    soln_interval::T
    ε::T
    derivative_error_order::Int
    hyper_diffusion_order::Int
    quad_nodes::Vector{T}
    quad_weights::Vector{T}
    lhs::LArray{Any, 1, Vector{Any}, (:mat, :colors, :cache)}
    rhs::LArray{Any, 1, Vector{Any}, (:mat, :colors, :cache)}
end


"""
    LinearProblem(n_fields; kwargs...)

# Keyword Arguments
- `solver::SolverMethod`=ArnoldiMethod() : Solver method used for linear solver
- `soln_interval::AbstractFloat`=8π : Interval in radians over which to compute linear solutions
- `ε::AbstractFloat`=400.0 : Numerical hyperdiffusion parameter
- `error_order::Integer`=4 : Error order for finite difference discretization
- `hyper_diffusion_order::Integer`=2 : Exponent of hyperdiffusion operator ``(iε)ⁿ∂ⁿ/∂ηⁿ``
- `quad_points::Integer`=1000 : Number of points using in Gauss-Legendre quadrature

# See also: (`SolverMethod`)[@ref]
"""
function LinearProblem(n_fields::Int = 3;
                       n_points = 1024,
                       solver=ArnoldiMethod(),
                       soln_interval=8π,
                       n_ev=1,
                       ε=400.0,
                       error_order=4,
                       hyper_diffusion_order=2,
                       quad_points=1024,
                       FloatType =Float64
                      )
    nodes, weights = gausslegendre(quad_points)
    rank = n_fields * n_points
    temp_sparse = spzeros(Complex{FloatType}, rank, rank)
    color_vec = matrix_colors(temp_sparse)
    sparse_cache = FiniteDiff.JacobianCache(rand(Complex{FloatType}, rank), colorvec = color_vec, sparsity = temp_sparse)
    sparse_op = LVector(mat = temp_sparse, colors = color_vec, cache = sparse_cache)
    return LinearProblem{n_fields, typeof(solver), FloatType, n_ev}(solver, n_points, soln_interval,
                                                                    ε, error_order, hyper_diffusion_order,
                                                                    convert.(FloatType, nodes), 
                                                                    convert.(FloatType, weights),
                                                                    deepcopy(sparse_op), deepcopy(sparse_op))
end
