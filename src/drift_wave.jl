
struct DriftWave{N, T, Fields}
    I::CartesianIndex
    k_perp::SpectralPoint{T}
    amplitude::T
    η_span::BoundsTuple{T}
    ω::SVector{N, Complex{T}}
    β::SLArray{Tuple{N}, SplineType, 1, N, Fields}
    norms::T
    k²::NamedTuple{(:perp, :parallel), Tuple{T, T}}
    coupling_matrix::SArray{Tuple{N, N}, SplineType, 2}
    inv_coupling_matrix::SArray{Tuple{N, N}, SplineType, 2}
end

function DriftWave(n_points::Int,
                   field_labels::NTuple{N, Symbol};
                  ) where {N}
    return DriftWave((0, 0), 0.0, 0.0, 1.0, -1.0π, 1.0π, n_points, field_labels)
end

function DriftWave()
    return DriftWave(1024, (:Φ, :U, :T))
end

function DriftWave(I, kx, ky, amp, η_span, ω, β, norm, k_perp2, k_parallel2, c_mat, inv_c_mat, field_labels)
    size(ω) == size(β) || throw(DimensionMismatch("Incompatible arrays of eigenvalues and eigenvectors"))
    size(c_mat, 1) == size(c_mat, 2) || throw(DimensionMismatch("Coupling matrix must be square"))
    size(c_mat, 1) == size(ω, 1) || throw(DimensionMismatch("Incompatible ranks between eigenvalues and coupling matrix"))
    N = size(ω, 1)
    T = eltype(real(ω))
    field_labels = field_labels
    if !(field_labels isa NTuple{N, Symbol})
        @warn "Incompatible number of field labels, setting field labels to (:a, :b, ...)"
        field_labels = (Symbol.(collect(range(start = 'a', step = 1, length = N))...))
    end
    return DriftWave{N, T, field_labels}(CartesianIndex(I),
                                         SpectralPoint{T}(kx, ky),
                                         amp, η_span, ω, β,
                                         norm, (perp = k_perp2, parallel = k_parallel2),
                                         c_mat, inv_c_mat)
end

function DriftWave(I, kx, ky, amp, η_min, η_max, nsteps_η, field_labels; FloatType = Float64)
    N = length(field_labels)
    ω = SVector{N, Complex{FloatType}}(zeros(Complex{FloatType}, N))
    η_range = range(start = η_min, length = nsteps_η, step = (η_max - η_min)/(nsteps_η - 1))
    η_span = (min = η_min, max = η_max)
    zero_itp = scale(interpolate(zeros(Complex{FloatType}, length(η_range)), BSpline(Cubic(Throw(OnGrid())))), η_range)
    β = SLArray{Tuple{N}, field_labels}(([zero_itp for field in field_labels]))
    norms = one(FloatType)
    k_perp2 = zero(FloatType)
    k_parallel2 = zero(FloatType)
    c_mat = SMatrix{N, N}([zero_itp for i in eachindex(1:N*N)]...)
    inv_c_mat = SMatrix{N, N}([zero_itp for i in eachindex(1:N*N)])
    return DriftWave(I, kx, ky, amp, η_span, ω, β, norms, k_perp2, k_parallel2, c_mat, inv_c_mat, field_labels)
end

# Extend the get index function to return
# the eigenvalue
function Base.getindex(u::DriftWave{N, T, Fields},
                        i::Int;
                       ) where {N, T, Fields}
    return getfield(u, :ω)[i]
end

function Base.getindex(u::DriftWave{N, T, Fields},
                       s::Symbol;
                      ) where {N, T, Fields}
    return getproperty(u, s)
end

# Extend the getproperty function to return
# the eigenvalue field_labels as well
function Base.getproperty(u::DriftWave{N, T, Fields},
                          s::Symbol;
                        ) where {N, T, Fields}
    return s in Fields ? view(getfield(u, :β), findfirst(f -> f == s, Fields))[] : getfield(u, s)
end

function n_fields(::DriftWave{N, T, Fields}) where {N, T, Fields}
    return N
end

function float_type(::DriftWave{N, T, Fields}) where {N, T, Fields}
    return T
end

function fields(::DriftWave{N, T, Fields}) where {N, T, Fields}
    return Fields
end

mutable struct DriftWaveBundle{N, T, E, Fields}
    k_perp::SpectralPoint{T}
    η_span::NamedTuple{(:min, :max), Tuple{T, T}}
    modes::Vector{DriftWave{N, T, Fields}}
    computed_solution::Bool

    DriftWaveBundle{N, T, E, Fields}(a, b, c, d) where {N, T, E, Fields} = new{N, T, E, Fields}(a, b, c, d)
    DriftWaveBundle{N, T, E, Fields}(n_points) where {N, T, E, Fields} = new{N, T, E, Fields}(SpectralPoint{T}(kx, ky),
                                                                                              (min = -1.0π, max = 1.0π),
                                                                                              fill(DriftWave((0, 0), SpectralPoint{T}(kx ,ky), one(T), -1.0π, 1.0π, n_points, Fields), N),
                                                                                              false)
    DriftWaveBundle() = DriftWaveBundle{3, Float64, 1, (:Φ, :U, :T)}(1024)
end

function DriftWaveBundle(modes::Vector{DriftWave{N, T, Fields}};
                         computed_solution = true,
                        ) where {N, T, Fields} 
    E = length(modes)
    k_perp = first(modes).k_perp
    η_span = first(modes).η_span
    return DriftWaveBundle{N, T, E, Fields}(k_perp, η_span, modes, computed_solution)
end


function DriftWaveBundle(modes::Vector{DriftWave{N, T, Fields}},
                         η_span::BoundsTuple;
                         computed_solution = true,
                        ) where {N, T, Fields} 
    E = length(modes)
    k_perp = first(modes).k_perp
    return DriftWaveBundle{N, T, E, Fields}(k_perp, η_span, modes, computed_solution)
end

function DriftWaveBundle(I::Union{CartesianIndex, NTuple{2, Int}},
                         kx::T,
                         ky::T,
                         amp::T,
                         η_min = -1.0π,
                         η_max = 1.0π,
                         nsteps_η::Int = 128; 
                         n_fields = 3, 
                         n_ev = 1, 
                         FloatType = Float64, 
                         field_labels = (:Φ, :U, :T),
                         computed_solution::Bool = false,
                        ) where {T}
    length(field_labels) == n_fields || throw(error("Number of field labels must match the number of field_labels"))
    modes = fill(DriftWave(CartesianIndex(I), kx, ky, amp, η_min, η_max, nsteps_η, field_labels; FloatType = FloatType), n_ev)
    return DriftWaveBundle(modes; computed_solution = computed_solution)
end

function Base.getindex(b::DriftWaveBundle{N, T, E, Fields},
                       i::Int;
                      ) where {N, T, E, Fields}
    return view(b.modes, i)[]
end

function Base.eachindex(b::DriftWaveBundle{N, T, E, Fields}) where {N, T, E, Fields}
    return eachindex(b.modes)
end

function fields(::DriftWaveBundle{N, T, E, Fields}) where {N, T, E, Fields}
    return Fields
end

function n_ev(::DriftWaveBundle{N, T, E, Fields}) where {N, T, E, Fields}
    return E
end

function float_type(::DriftWaveBundle{N, T, E, Fields}) where {N, T, E, Fields}
    return T
end
