function compute_linear_spectrum!(model::FluidModel{N, T};
                                  restart::Bool = false,
                                 ) where {N, T}
    kx_range = 1:n_kx_modes(model.grid, neg_kx = false)
    ky_range = 1:n_ky_modes(model.grid)
    initial_ky_index = initial_guess(model)[2]
    start_ky_index = initial_ky_index
    start_kx_index = 1
    scan_dir = :down
    if restart
        restart_ky_index = div(findfirst(m -> m.computed_solution, model.spectrum)[2] - 1, n_kx_modes(model.grid, neg_kx = false)) + 1
        restart_kx_index = findfirst(i -> !model.spectrum[i, restart_ky_index].computed_solution, 1:n_kx_modes(model.grid, neg_kx = false))
        if isnothing(restart_kx_index)
            if restart_ky_index > inital_ky_index
                restart_ky_index += 1
                scan_dir = :up
            elseif restart_ky_index < inital_ky_index
                restart_ky_index -= 1
            end
            restart_kx_index = 1
        end
        start_ky_index = restart_ky_index
        start_kx_index = restart_kx_index
@debug start_kx_index, start_ky_index
    end
            

    #rank = N * model.problem.n_points
    #lhs = spzeros(Complex{T},rank,rank)
    #rhs = spzeros(Complex{T},rank,rank)+I

    init = !restart ? true : false
    
    if scan_dir === :down
        for ky_index in start_ky_index:-1:1
            for kx_index in (ky_index == start_ky_index ? start_kx_index : 1):1:last(kx_range)
                @debug "Computing linear solution at (kx,ky) = ($kx_index, $ky_index)"
                compute_linear_mode!(model, CartesianIndex(kx_index, ky_index), init=init, scan_dir=scan_dir)
                if init
                    init = false
                end
            end
        end
        scan_dir = :up
    end

    for ky_index in (start_ky_index > initial_ky_index ? start_ky_index : initial_ky_index + 1):1:last(ky_range)
        for kx_index in (ky_index == start_kx_index ? start_kx_index : 1):1:last(kx_range)
            @debug "Computing linear solution at (kx,ky) = ($kx_index, $ky_index)"
            compute_linear_mode!(model, CartesianIndex(kx_index, ky_index), init=init, scan_dir=:scan_dir)
        end
    end
end


function compute_linear_mode!(model::FluidModel{N, T},
                              I ::CartesianIndex;
                              init::Bool=false,
                              scan_dir::Symbol=:none
                             ) where {N, T}

  ω_target, β_target = get_target(I, model, scan_dir, init=init)

  if imag(ω_target) > zero(T) || init
    # Solve for the eigenmodes
    if iszero(nnz(model.problem.lhs.mat))
        setup_sparse_operators!(model.problem)
    end
    ωs, βs = compute_eigenmodes(I, model; target = ω_target, init_vec = β_target)
    # Sort the modes
    #ω_index = sort_modes(kx,ky,pointRange,ωs,βs,geomView,model.plasma,model.problem)
    #β_vec = βs[:,ω_index]./maximum(abs.(βs[:,ω_index]))

    # Update the spectrum
    update_spectrum!(model, I, ωs, βs)
  else
    update_spectrum!(model, I, zeros(Complex{T}, N), nothing)
  end
end

function compute_linear_mode!(model::FluidModel{N, T},
                              inds::Union{AbstractVector{Int}, NTuple{2, Int}};
                              kwargs...
                             ) where {N, T}
    length(inds) == 2 || throw(DimensionMismatch("Require two wave number indicies, received $(length(inds))"))
    compute_linear_mode!(model, CartesianIndex(inds[1], inds[2]); kwargs...)
end


function update_spectrum!(model::FluidModel{N, T},
                          I::CartesianIndex,
                          ωs::AbstractVector{Complex{T}},
                          βs::Union{AbstractArray{Complex{T}},Nothing};
                         ) where {N, T}
  n_points = model.problem.n_points

  if imag(ωs[1]) > zero(T)

    mode_bundle = model.spectrum[I]
    η_range = range(start = mode_bundle.η_span.min, stop = mode_bundle.η_span.max, length = n_points)
    itp(A) = scale(interpolate(A, BSpline(Cubic(Throw(OnGrid())))), η_range)
    evecs = Vector{SplineType}(undef, N)
    modes = Vector{DriftWave{N, T, fields(mode_bundle)}}(undef, n_ev(mode_bundle))
    β_sq = Vector{T}(undef, n_points)
    β_sq_cumsum = similar(β_sq)
    for mode_index in 1:n_ev(mode_bundle)
        for field_index in 1:N
            first_index = n_points * (field_index - 1) + 1
            last_index = n_points * field_index
            evecs[field_index] = itp(βs[first_index:last_index, mode_index]./maximum(abs.(βs[first_index:last_index, mode_index])))
        end
        β_sq .= abs2.(βs[1:n_points, mode_index])
        cumsum!(β_sq_cumsum, β_sq)
        β_sq_cumsum ./= last(β_sq_cumsum)
        η_min = η_range[findfirst(i -> i > T(0.01), β_sq_cumsum)]
        η_max = η_range[findlast(i -> i < T(0.99), β_sq_cumsum)]
        norm_val = vec_norm(mode_bundle.η_span, evecs[1], model.geometry, model.problem)
        ω0, k_perp², k_par² = marginal_mode(ωs[mode_index], model.grid.k_perp[I], 
                                            mode_bundle.η_span, evecs[1],
                                            model.geometry, model.plasma, model.problem)
        roots = SVector{3, Complex{T}}(ωs[mode_index], conj(ωs[mode_index]), ω0)
        c_mat, inv_c_mat = coupling_matrix(model.grid.k_perp[I], mode_bundle.η_span,
                                           roots, evecs, model.problem, model.plasma)
        amp = power_spectrum_coeff(I, model.grid)
        modes[mode_index] = DriftWave(I, model.grid.k_perp.kx[I], model.grid.k_perp.ky[I],
                                       amp, (min = η_min, max = η_max), roots,
                                       SLArray{Tuple{N}, fields(mode_bundle)}(evecs...), 
                                       norm_val, k_perp²/norm_val, k_par²/norm_val,
                                       c_mat, inv_c_mat, fields(mode_bundle))
    end
    model.spectrum[I] = DriftWaveBundle(modes, mode_bundle.η_span; computed_solution = true)

    if I[1] > 1
      J = CartesianIndex(n_kx_modes(model.grid) - I[1] + 2, I[2])
      evecs = Vector{SplineType}(undef, N)
      modes = Vector{DriftWave{N, T, fields(model.spectrum[I])}}(undef, n_ev(model.spectrum[I]))
      η_range = range(start = -model.spectrum[I].η_span.max, stop = -model.spectrum[I].η_span.min, length = n_points)
      for mode_index in 1:n_ev(mode_bundle)
        for field_index in 1:N
            evecs[field_index] = scale(interpolate(reverse(model.spectrum[I][mode_index].β[field_index].itp.coefs.parent[2:end-1]),
                                                   BSpline(Cubic(Throw(OnGrid())))), η_range)
        end
        roots = model.spectrum[I][mode_index].ω
        η_span = (min = -model.spectrum[I][mode_index].η_span.max, max = -model.spectrum[I][mode_index].η_span.min)
        norm_val = model.spectrum[I][mode_index].norms
        k² = model.spectrum[I][mode_index].k²
        c_mat, inv_c_mat = coupling_matrix(model.grid.k_perp[J], η_span,
                                           roots, evecs, model.problem, model.plasma)
        amp = power_spectrum_coeff(J, model.grid)
        modes[mode_index] = DriftWave(J, -model.grid.k_perp.kx[J], model.grid.k_perp.ky[J], amp,
                                      η_span, roots, evecs, norm_val, k².perp, k².parallel,
                                      c_mat, inv_c_mat, fields(model.spectrum[I]))
      end

      model.spectrum[J] = DriftWaveBundle(modes, (min = first(η_range), max = last(η_range)); computed_solution = true)
    end
  else
    model.spectrum[I] = DriftWaveBundle(I, model.grid.k_perp.kx[I], model.grid.k_perp.ky[I], one(T),
                                        model.spectrum[I].η_span.min, model.spectrum[I].η_span.max,
                                        model.problem.n_points; n_fields = N, 
                                        n_ev = n_ev(model.spectrum[I]), FloatType = T,
                                        field_labels = fields(model.spectrum[I]), computed_solution = true)
    if I[1] > 1
        J = CartesianIndex(n_kx_modes(model.grid) - I[1] + 1, I[2])
        model.spectrum[J] = DriftWaveBundle(J, model.grid.k_perp.kx[J], model.grid.k_perp.ky[J], one(T),
                                            -model.spectrum[I].η_span.max, -model.spectrum[I].η_span.min,
                                            model.problem.n_points; n_fields = N, 
                                            n_ev = n_ev(model.spectrum[I]), FloatType = T,
                                            field_labels = fields(model.spectrum[I]), computed_solution = true)
    end
  end
end

function get_target(I::CartesianIndex,
                    model::FluidModel{N, T},
                    scan_dir::Symbol=:down;
                    init::Bool=false
                   ) where {N, T}

  if init
    ι = model.geometry.parameters.ι
    nfp = model.geometry.parameters.nfp
    σ = nfp > 1 ? π/abs(ι-nfp) : 0.4π
    ϕ = Vector{T}(undef, model.problem.n_points)
    η_range = range(start = model.spectrum[I].η_span.min, stop = model.spectrum[I].η_span.max, length = model.problem.n_points)
    map!(η -> gaussian_mode(η, σ), ϕ, η_range)
    vec = scale(interpolate(ϕ, BSpline(Cubic(Throw(OnGrid())))), η_range)

    ω = compute_linear_roots(model.grid.k_perp[I], model.spectrum[I].η_span,
                             vec, model.geometry,
                             model.plasma, model.problem)
    return convert(Complex{T}, ω[1]), rand(Complex{T}, N * model.problem.n_points)
  else
    if scan_dir === :none
      return zero(Complex{T}), rand(Complex{T}, N * model.problem.n_points)
    else
      prev_index = I - CartesianIndex(1,0)
      if prev_index.I[1] < 1
        prev_index = scan_dir == :down ? I + CartesianIndex(0,1) : I - CartesianIndex(0,1)
      end
      ω_target = model.spectrum[prev_index][1][1]
      β = model.spectrum[prev_index][1].β
      β_target = Vector{Complex{T}}(undef, N * model.problem.n_points)
      for i in 1:N
        β_target[model.problem.n_points * (i-1) + 1:i * model.problem.n_points] .= β[i].itp.coefs.parent[2:end-1]
      end
      return ω_target, β_target
    end
  end
end

function initial_guess(model::FluidModel{N, T},
                      ) where {N, T}
  ky_indices = axes(model.grid.k_perp, 2)
  median_ky_index = convert(Int, ceil(median(ky_indices)))
  ι = model.geometry.parameters.ι
  nfp = model.geometry.parameters.nfp
  σ = nfp > 1 ? π/abs(ι-nfp) : 0.4π
  ϕ = Vector{T}(undef, model.problem.n_points)
  η_range = range(start = first(model.spectrum).η_span.min, stop = first(model.spectrum).η_span.max, length = model.problem.n_points)
  map!(η -> gaussian_mode(η, σ), ϕ, η_range)
  vec = scale(interpolate(ϕ, BSpline(Cubic(Throw(OnGrid())))), η_range)

  for I in CartesianIndices((1, median_ky_index:n_ky_modes(model.grid)))
    ω = compute_linear_roots(model.grid.k_perp[I], model.spectrum[I].η_span,
                             vec, model.geometry,
                             model.plasma, model.problem)
    if imag(ω[1]) > 0
      return I
    end
  end

  for I = CartesianIndices((1, median_ky_index-1:-1:1))
    ω = compute_linear_roots(model.grid.k_perp[I], model.spectrum[I].η_span,
                             vec, model.geometry,
                             model.plasma, model.problem)
    if imag(ω[1]) > 0
      return I
    end
  end

  # If we are here, something has probably gone wrong, but try it anyway
  return median_ky_index
end
