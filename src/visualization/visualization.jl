#using .CairoMakie
#using .GLMakie
using LaTeXStrings
using Printf

include("growth_contour.jl")
include("mode_spectrum.jl")
include("mode_structure.jl")
include("tau_contour.jl")
include("marg_contour.jl")
