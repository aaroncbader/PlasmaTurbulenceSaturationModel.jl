#using DelimitedFiles, CairoMakie, GLMakie
#GLMakie.activate!()
#using CairoMakie
#using LaTeXStrings

function plotTauContours(τ::AbstractMatrix{T},
                           model::PlasmaTurbulenceSaturationModel.FluidModel;
                           resolution=(700,600),
                           fontsize=25,
                           colormap=:vik,
                           clevels=25,
                           colorrange = nothing,
                  			title= "Growth rates",
                            negativeModes=false,
                           label=L"|\mathrm{Re}(\tau C)|",
                          )  where {T}
  kx = PlasmaTurbulenceSaturationModel.kx(model,negativeModes=false)
  ky = PlasmaTurbulenceSaturationModel.ky(model)

  f = Figure(resolution=resolution, font = "CMU Serif")

  #fontsize_theme = Theme(fontsize = fontsize)
  #set_theme!(fontsize_theme)

  Axis(f[1, 1], xlabel = L"k_x\rho_s", ylabel = L"k_y\rho_s",xlabelsize=fontsize, ylabelsize=fontsize)
  co = contourf!(kx, ky, τ, levels = clevels, colormap = colormap)

  Colorbar(f[1, 2], co, label=label)

  f

end

function plot_heatmap(τ::AbstractMatrix{T},
                      model::PlasmaTurbulenceSaturationModel.FluidModel;
                      resolution=(900,600),
                      fontsize=25,
                      colormap=:inferno,
                      clevels=25,
                      colorrange = nothing,
                      title= "",
                      label=L"\mathrm{Re}(\tau C)",
                      font = "new computer modern roman"
                     )  where {T}
    kx = model.grid.k_perp.kx[1:size(τ, 1), 1]
    ky = model.grid.k_perp.ky[1, 1:size(τ, 2)]

    f = Figure(resolution=resolution, font = font)

    fontsize_theme = Theme(fontsize = fontsize, font = font)
    set_theme!(fontsize_theme)
    gl = GridLayout(f[1, 1])
    ax = Axis(gl[1, 1], xlabel = L"k_x\rho_s", ylabel = L"k_y\rho_s",
         xlabelsize=fontsize, ylabelsize=fontsize,
         title = title, titlefont = font, titlesize = fontsize,
         aspect = AxisAspect(abs(last(kx)/last(ky))))
    co = heatmap!(ax, kx, ky, τ,
                  levels = clevels, colormap = colormap,
                  colorrange = isnothing(colorrange) ? (minimum(filter(!isinf, τ)), maximum(τ)) : colorrange)

    Colorbar(gl[1, 2], co, label=label)
    tightlimits!(ax)
    colgap!(gl, 5)
    f

end
