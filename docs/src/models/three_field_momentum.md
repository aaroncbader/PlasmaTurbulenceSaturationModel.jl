The equation for the parallel velocity is given by
```math
m_i n_i \frac{\partial }{\partial t}\hat{\mathbf{b}} \cdot\mathbf{v} + m_i n_i \hat{\mathbf{b}}  \cdot \left(\mathbf{v}_E + \mathbf{v}_{i\ast}\right)\cdot \nabla\mathbf{v} = -\hat{\mathbf{b}}  \cdot \nabla p_i - e n_i \hat{\mathbf{b}}  \cdot \nabla\phi_1  - \hat{\mathbf{b}}  \cdot \nabla\cdot \Pi.
```
The parallel component of the divergence of the viscosity tensor vanishes at lowest order in ``\frac{\rho_\mathrm{s}}{a}`` in the collisionless
limit, thus the linear question equation for the parallel velocity is given by
```math
m_i n_i \frac{\partial v_\parallel}{\partial t} = - \hat{\mathbf{b}} \cdot\nabla p_i - e n_i \hat{\mathbf{b}} \cdot \nabla\phi_1.
```
Applying the Gene normalizations, we have
```math
\begin{aligned}
m_i n_{e0} \hat{n}_0 \frac{c_s}{a} \frac{\partial }{\partial \tau}c_s \frac{\rho_\mathrm{s}}{a}\hat{v}_\parallel &= - \frac{n_{e0} T_{e0}\hat{n}_0}{a\sqrt{g}\hat{\mathbf{B}} }\frac{\partial }{\partial \theta} \frac{\rho_\mathrm{s}}{a}\left(\hat{T}_1 + \frac{\hat{T}_0}{\hat{n}_0}\hat{n}_1\right) - \frac{n_{e0}T_{e0}\hat{n}_0}{a}\frac{1}{\sqrt{g}\hat{\mathbf{B}} }\frac{\partial }{\partial \theta}\frac{\rho_\mathrm{s}}{a}\hat{\phi}_1 \\
\hat{n}_0\frac{n_{e0}c_s^2}{a}\frac{\rho_\mathrm{s}}{a}\frac{\partial \hat{v}_\parallel}{\partial \tau} &= -\hat{n}_0\frac{n_{e0}c_s^2}{a}\frac{\rho_\mathrm{s}}{a}\frac{1}{\sqrt{g}\hat{\mathbf{B}} }\frac{\partial }{\partial \theta}\left(\hat{T}_1 + \frac{\hat{T}_0}{\hat{n}_0}\hat{n}_1 + \hat{\phi}_1\right).
\end{aligned}
```
In the cold ion limit, ``\hat{T}_0/\hat{n}_0 \rightarrow 0`` thus we have for the parallel velocity evolution
```math
\frac{\partial \hat{v}_\parallel}{\partial \tau} + \frac{1}{\sqrt{g}\hat{\mathbf{B}} }\frac{\partial }{\partial \theta}\left(\hat{\phi}_1 + \hat{T}_1\right) = 0.
```
