The background temperature gradient can be represented by
```math
\epsilon_T = -\frac{a}{T_{e0}}\nabla T_0 = -\frac{a}{T_{e0}}\frac{T_{e0}}{a}\hat{\nabla} \hat{T}_0 = -\frac{\partial \hat{T}_0}{\partial x^1}.
```
The operator ``\hat{\nabla}_\perp^2 f`` is equal to ``\hat{\nabla}_\perp \cdot \hat{\nabla}_\perp f``, where ``f(x^1,x^2,x^3)`` can be expressed as
```math
\begin{aligned}
\hat{\nabla}_\perp \cdot \hat{\nabla}_\perp f &= \frac{\partial^2 f}{\partial \left(x^1\right)^2}\hat{\nabla} x^1 \cdot \hat{\nabla} x^1  + 2  \frac{\partial f}{\partial x^1} \frac{\partial f}{\partial x^2} \hat{\nabla} x^1 \cdot \hat{\nabla} x^2 + \frac{\partial^2 f}{\partial \left(x^2\right)^2} \hat{\nabla} x^2 \cdot \hat{\nabla} x^2 \\
&= \frac{\partial^2 f}{\partial \left(x^1\right)^2} g^{11} + 2 \frac{\partial f}{\partial x^1}\frac{\partial f}{\partial x^2} g^{12} + \frac{\partial^2 f}{\partial \left(x^2\right)^2} g^{22}
\end{aligned}
```
For clarity, we will make the following variable substitutions: ``x^1 \rightarrow x``, ``x^2 \rightarrow y``, ``x^3 \rightarrow z``.
Assuming a WKB-like solution, where the fluctuations have short perpendicular variation but long parallel variation, fluctuating
quantities ``\hat{f}_1`` can be represented as
```math
\hat{f}_1(x,y,z,t) = \sum_{k_x,k_y} \tilde{f}_k(z,t) \exp\left(i k_x x + i k_y y\right),
```
allowing us to transform the partial derivatives ``\frac{\partial \hat{f}_1}{\partial x^1} \rightarrow i k_x \tilde{f}_k``, ``\frac{\partial \hat{f}_1}{\partial x^2}
\rightarrow i k_y \tilde{f}_k``.  Thus we can write the perpendicular Laplacian operator as
```math
\hat{\nabla}_\perp^2 \hat{f}_1 = - \left(k_x^2 g^{11} + 2 k_x k_y g^{12} + k_y^2 g^{22}\right) \tilde{f} = - B_k \tilde{f}.
```
Similarly, the magnetic curvature term can be written as
```math
\hat{\mathbf{b}} \times \mathbf{\hat{\kappa}} \cdot \hat{\nabla} \hat{f}_1 = i \left(k_x \mathcal{K}_1 + k_y \mathcal{K}_2\right) \tilde{f} =  i D_k
\tilde{f}.
```
This gives the final system of equations governing the linear evolution of fluctuations:
```math
\begin{aligned}
\frac{\partial }{\partial \tau}\left[\left(1+B_k\right)\tilde{\phi} + B_k\tilde{T}\right] + i \frac{2}{\hat{\mathbf{B}} }D_k\left(\tilde{\phi} + \tilde{T}\right) + \frac{1}{\sqrt{g}\hat{\mathbf{B}} }\frac{\partial \tilde{v}_\parallel}{\partial z} &= 0 \\
\frac{\partial \tilde{v}_\parallel}{\partial \tau} + \frac{1}{\sqrt{g}\hat{\mathbf{B}} }\frac{\partial }{\partial z}\left(\tilde{\phi} + \tilde{T}\right) &= 0 \\
\frac{\partial \tilde{T}}{\partial \tau} + i \frac{k_y \epsilon_T}{\hat{\mathbf{B}} }\tilde{\phi} &= 0
\end{aligned}
```
For linear problems, we will assume Fourier time dependence, such that ``\frac{\partial }{\partial \tau} \rightarrow - i \omega``, allowing us to write the
linear system as a generalized eigenvalue problem ``\mathbf{A}\cdot\mathbf{x} = \omega \mathbf{B}\cdot \mathbf{x}``, where ``\omega`` is the eigenvalue and
``\mathbf{x}`` is the eigenvector given by ``\left(\tilde{\phi},\tilde{v}_\parallel,\tilde{T}\right)^T``.
```math
\begin{aligned}
\frac{2}{\hat{\mathbf{B}} (z)}D_k(z)\left(\tilde{\phi}(z) + \tilde{T}(z)\right) - \frac{i}{\sqrt{g}(z)\hat{\mathbf{B}} (z)}\frac{\partial \tilde{v}_\parallel(z)}{\partial z} &=
\omega\left[\left(1+B_k(z)\right)\tilde{\phi}(z) + B_k(z)\tilde{T}(z)\right] \\
-\frac{i}{\sqrt{g}(z)\hat{\mathbf{B}} (z)}\frac{\partial }{\partial z}\left(\tilde{\phi}(z)+\tilde{T}(z)\right) &= \omega \tilde{v}_\parallel(z) \\
\frac{k_y\epsilon_T}{\hat{\mathbf{B}} (z)}\tilde{\phi}(z) &= \omega \tilde{T}(z)
\end{aligned}
```
