The energy equation is given by
```math
\frac{3}{2}n_i\frac{\partial T_i}{\partial t} + \frac{3}{2}n_i\left(\mathbf{v}_E + \mathbf{v}_{i\ast}\right)\cdot\nabla T_i + n_i T_i\nabla\cdot\mathbf{v} +
\nabla\cdot q_i = 0.
```
Using the cold ion approximation, the divergence of the velocity can be neglected.  The heat flux is given by the diamagnetic heat
flux
```math
q_i = \frac{5}{2}\frac{n_i T_i}{e B}\hat{\mathbf{b}}  \times \nabla T_i,
```
which vanishes in the cold ion limit.  The resulting evolution equation for ``T_1`` can be re-written as
```math
\frac{3}{2}n_{e0} T_{e0} \hat{n}_0 \frac{c_s}{a}\frac{\rho_\mathrm{s}}{a}\frac{\partial \hat{T}_1}{\partial \tau} + \frac{3}{2}
n_{e0}T_{e0}\hat{n}_0\left(c_s\frac{\rho_\mathrm{s}}{a}\frac{1}{\hat{\mathbf{B}} }\hat{\mathbf{b}} \times\hat{\nabla}\hat{\phi}_1 +
c_s\frac{\rho_\mathrm{s}}{a}\frac{1}{\hat{\mathbf{B}} }\hat{\mathbf{b}} \times\left(\hat{\nabla}\hat{T}_0 + \hat{\nabla}\hat{T}_1\right)\right)\cdot\left(\frac{\hat{\nabla}}{a}\hat{T}_0 +
\frac{\hat{\nabla}}{\rho_\mathrm{s}}\frac{\rho_\mathrm{s}}{a}\hat{T}_1\right) = 0.
```
Canceling the coefficients and neglecting the nonlinear term due to ``E \times B`` advection, we have
```math
\begin{aligned}
&\frac{\partial \hat{T}_1}{\partial \tau} + \frac{1}{\hat{\mathbf{B}} }\hat{\mathbf{b}} \times\hat{\nabla}\hat{\phi}_1 \cdot \hat{T}_0 + \frac{1}{\hat{\mathbf{B}} }\left(\hat{\mathbf{b}} \times \hat{\nabla} \hat{T}_0 \cdot \hat{\nabla} \hat{T}_1 + \hat{\mathbf{b}} \times\hat{\nabla}\hat{T}_1 \cdot \hat{\nabla}\hat{T}_0\right) = 0  \\
&\frac{\partial \hat{T}_1}{\tau} + \frac{1}{\hat{\mathbf{B}} }\hat{\mathbf{b}} \times\hat{\nabla}\hat{\phi}_1 \cdot \hat{T}_0 + \frac{1}{\hat{\mathbf{B}} }\left(\hat{\mathbf{b}} \times\hat{\nabla}\hat{T}_0 \cdot \hat{\nabla} \hat{T}_1 - \hat{\mathbf{b}}  \times\hat{\nabla}\hat{T}_0 \cdot \hat{\nabla} \hat{T}_1\right) = 0 \\
&\frac{\partial \hat{T}_1}{\partial \tau} - \frac{1}{\hat{\mathbf{B}} }\frac{\partial \hat{\phi}_1}{\partial x^2}\frac{\partial \hat{T}_0}{\partial x^1} = 0.
\end{aligned}
```
