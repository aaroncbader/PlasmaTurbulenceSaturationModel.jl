# Normalizations
To make use of the machinery and work that has been developed for the [GENE](https://genecode.org) gyrokinetics code, GENE normalizations will be used.  The GENE normalizations are given by:
```math
\begin{matrix}
(x,y) \rightarrow \rho_\text{ref}(\hat{x},\hat{y}) & z \rightarrow \hat{z} \\
v_\parallel \rightarrow c_\text{ref} v_{T \sigma} \hat{v_\parallel} & \mu \rightarrow \frac{T_\text{ref}}{B_\text{ref}} T_{\sigma 0} \hat{\mu} \\
t \rightarrow \frac{L_\text{ref}}{c_\text{ref}} \tau & B_0 \rightarrow B_\text{ref} B_0 \\
q_\sigma \rightarrow e \hat{q_\sigma} & m_\sigma \rightarrow m_\text{ref} \hat{m_\sigma} \\
n_{\sigma 0} \rightarrow n_{ref} \hat{n_{\sigma 0}} & T_{\sigma 0} \rightarrow T_\text{ref} \hat{T_{\sigma 0}}
\end{matrix}, 
```
For purposes of this model, we will use ``L_\text{ref} = a``, ``c_\text{ref} = c_\text{s}`` and ``T_\text{ref} = T_{\text{e}0}``. The derivative operators are normalized differently depending on whether they are acting on equilibrium or fluctuating quantities:
```math
\nabla A_0 = \frac{1}{a}\hat{\nabla} A_0,\,\,\,\nabla A_1 = \frac{1}{\rho_\text{s}}\hat{\nabla} A_1
```
where ``A_0`` is an equilibrium quantity and ``A_1`` is a perturbation.
The magnetic geometry employed in GENE and calculated by GIST is given by
```math
\mathbf{B} = \nabla \Phi(s) \times \nabla (q\theta - \zeta),
```
where ``\Phi`` is the *poloidal* flux and ``s`` is a flux surface label defined by the *toroidal* flux:
```math
s = \frac{\Psi}{\Psi_{edge}},\,\,\,\Psi_{edge} = \frac{B_a a^2}{2}
```
where ``B_a`` is the magnetic field at the last closed flux surface and ``a`` is the average minor radius. The flux tube domain in GENE is expressed in terms of the normalized coordinates
```math
x^1 = \sqrt{s},x^2 = \frac{\sqrt{s_0}}{q_0}(q(\theta - \theta_k) - \zeta),x^3 = \theta
```
where ``\theta`` is the geometric poloidal angle and for simplicity we will assume ``\theta_k = 0``.  The quantities with the "0" subscript are quantities evaluated on the flux surface in question.  Using ``\frac{d\Psi}{ds} = q \frac{d\Phi}{ds}`` and ``\nabla \Phi(s) = \frac{d\Phi}{ds}\nabla s``, we can write Eq.~(eq:Bpolflux) as
```math
\mathbf{B}= \frac{2\sqrt{s}}{q}\frac{d\Psi}{ds}\nabla \sqrt{s}\times\nabla (q\theta-\zeta)
```
In the GIST formulation of the magnetic geometry, the coefficient ``\frac{\sqrt{s}}{q}`` is expressed as ``\frac{\sqrt{s_0}}{q_0}`` and absorbed into the field line label.  Since the gradients are acting on geometric quantities, the are normalized by the minor radius: ``\nabla = \frac{1}{a}\hat{\nabla}``.  Using ``\frac{d\Psi}{ds} = \Psi_{edge} = \frac{B_a a^2}{2}`` we can re-express the magnetic field as
```math
\mathbf{B} = B_a \hat{\nabla} x^1 \times \hat{\nabla} x^2.
```
GIST calculates geometric quantities using the normalized magnetic field ``\hat{\mathbf{B}} = \mathbf{B}/B_a`` and thus we can see that
```math
\hat{\mathbf{B}} = \hat{\nabla} x^1 \times \hat{\nabla} x^2.
```
The various geometric elements for the model will be computed using these normalized quantities.  The metric elements are given by
```math
g^{11} = \hat{\nabla} x^1 \cdot \hat{\nabla} x^1,\,\,\, g^{12} = \hat{\nabla} x^1 \cdot \hat{\nabla} x^2,\,\,\, g^{22} = \hat{\nabla} x^2 \cdot \hat{\nabla} x^2.
```
The magnetic field strength is specified by ``\hat{B}`` and the scalar Jacobian is given by
```math
\sqrt{g} = \frac{1}{\hat{\nabla} x^1 \cdot \hat{\nabla} x^2 \times \hat{\nabla} x^3}.
```
The curvature operator due to an spatially inhomogeneous magnetic field acting on a contravariant vector quantity ``\mathbf{A} = A_1 \hat{\nabla}x^1 + A_2 \hat{\nabla}x^2 + A_3 \hat{\nabla}x^3`` is given by 
```math
\frac{\mathbf{A}\cdot\mathbf{\hat{B}}\times\hat{\nabla}\hat{B}}{\hat{B}^2} = A_1 \underbrace{\left(-\frac{\partial \hat{B}}{\partial x^3}\frac{g^{11}g^{23}-g^{12}g^{13}}{\hat{B}^2} -\frac{\partial \hat{B}}{\partial x^2}\right)}_\mathcal{K_1} + A_2\underbrace{\left(\frac{\partial\hat{B}}{\partial x^1} +\frac{\partial \hat{B}}{\partial x^3}\frac{g^{22}g^{13}-g^{23}g^{12}}{\hat{B}^2}\right)}_\mathcal{K_2} = A_1 \mathcal{K}_1 + A_2 \mathcal{K}_2,
```
where ``\mathcal{K}_1`` and ``\mathcal{K}_2`` are the traditional notation used in GENE.  We can also express the normalized perpendicular Laplacian operator acting on a scalar quantity ``f`` in terms of the metric coefficients:
```math
\hat{\nabla}_\perp \cdot \hat{\nabla}_\perp f = \hat{\nabla}_\perp^2 f = \hat{\nabla}x^1 \cdot \hat{\nabla}x^1 \frac{\partial^2 f}{\partial \left(x^1\right)^2} + 2 \hat{\nabla}x^1\cdot \hat{\nabla}x^2 \frac{\partial^2 f}{\partial x^1 \partial x^2} + \hat{\nabla}x^2\cdot \hat{\nabla}x^2 \frac{\partial^2 f}{\partial \left(x^2\right)^2} = g^{11}\frac{\partial^2 f}{\partial \left(x^1\right)^2} + 2g^{12}\frac{\partial^2 f}{\partial x^1 \partial x^2} + g^{22}\frac{\partial^2 f}{\partial \left(x^2\right)^2}.
```
The parallel derivative operator is expressed as 
```math
\hat{\mathbf{b}} \cdot \hat{\nabla} = \frac{1}{\sqrt{g}\hat{B}}\frac{\partial }{\partial \theta}.
```
To be consistent with the GENE normalizations, the following perturbation expansions will be used:
```math
\begin{aligned}
\phi_1 &= \frac{T_{\mathrm{e}0}}{e}\frac{\rho_\text{s}}{a}\hat{\phi}_1\left(x^1,x^2,x^3,\tau\right)\\
T_\mathrm{i} &= T_0 + T_1 = T_{\mathrm{e}0}\left(\hat{T}_0(x^1) + \frac{\rho_\text{s}}{a}\hat{T}_1\left(x^1,x^2,x^3,\tau\right)\right)\\
n_\mathrm{i} &= n_0 + n_1 = n_{\mathrm{e}0}\left(\hat{n}_0(x^1) + \frac{\rho_\text{s}}{a}\hat{n}_1\left(x^1,x^2,x^3,\tau\right)\right)\\
v_\parallel &= c_s \frac{\rho_\text{s}}{a} \hat{v}_\parallel(x^1,x^2,x^3,t)
\end{aligned}
```
This assumes there is both no macroscopic electric field and no macroscopic parallel flow.  We can define a characteristic gyrofrequency using the magnetic field at the last closed flux surface and normalize the parallel velocity to the ion acoustic speed:
```math
\Omega_i = \frac{e B_a}{m_i},\,\,\,c_s = \sqrt{T_{e0}/m_i},\,\,\,\rho_\text{s} = c_s/\Omega_i.
```
We will also normalize macroscopic gradients to the minor radius:
```math
\epsilon_T = -\frac{a}{T_{e0}}\nabla T_0 = -\frac{T_{e0} a}{T_{e0}} \frac{\hat{\nabla}}{a}\hat{T}_0 = -\frac{\partial \hat{T}_0}{\partial x^1},
```
```math
\epsilon_n = -\frac{a}{n_{e0}}\nabla n_0 = -\frac{n_{e0} a}{n_{e0}} \frac{\hat{\nabla}}{a}\hat{n}_0 = -\frac{\partial \hat{n}_0}{\partial x^1}.
```

