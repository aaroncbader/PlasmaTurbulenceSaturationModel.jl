## Fluid model of ITG turbulence in Stellarators
The collionless, source-free plasma two-fluid equations will be used to model ion scale turbulence in stellarator configurations.   The two-fluid equations are given by
```math
\text{Density conservation:}\,\,\frac{\partial n_\alpha}{\partial t}  = - \nabla\cdot \left(n_\alpha \mathbf{u}^\alpha\right),
```
```math
\text{Parallel momentum conservation:}\,\,\frac{\partial}{\partial t}\left(m_\alpha n_\alpha \mathbf{u}^\alpha\right) = - \nabla\cdot\left(m_\alpha n_\alpha \mathbf{u}^\alpha \mathbf{u}^\alpha + \Pi^\alpha\right) - \nabla p_\alpha + e_\alpha n_\alpha \left(\mathbf{E} + \mathbf{u}^\alpha \times \mathbf{B}\right),
```
```math
\text{Energy conservation:}\,\,\frac{\partial p_\alpha}{\partial t} = -\mathbf{u}^\alpha \cdot \nabla p_\alpha - \gamma p_\alpha \nabla \cdot\mathbf{u}^\alpha - \frac{2}{3}\Pi^\alpha : \nabla \mathbf{u}^\alpha -\frac{2}{3}\nabla\cdot\mathbf{q}^\alpha.
```
Here, ``\Pi`` is the dissipative pressure tensor, ``\mathbf{q}^\alpha`` is the heat flux and ``\gamma`` is the adiabatic index (ratio of specific heats).  These equations are coupled to the macroscopic Maxwell's equations:
```math
\nabla\cdot\mathbf{E} = \frac{\rho}{\epsilon_0},
```
```math
\nabla\times\mathbf{E} = -\frac{\partial \mathbf{B}}{\partial t},
```
```math
\nabla\times\mathbf{B} = \mu_0\mathbf{j} + \mu_0 \epsilon_0 \frac{\partial \mathbf{E}}{\partial t},
```
```math
\nabla\cdot\mathbf{B} = 0,
```
where ``\rho`` is the charge density and ``\mathbf{j}`` is the current density.

We will address turbulence by describing deviations around a background state, which is assumed to be in equilibrium, so that quantities are represented by an expansion: ``f = f_0 + f_1 + f_2 + \ldots`` where ``f_0`` is the equilibrium state.  This will be accomplished by making an ordering assumptions on different quantities:
 - Strongly magnetized plasma:
```math
\epsilon = \frac{\rho_\text{s}}{L_M} \ll 1
```
 - Small fluctuation amplitudes: 
```math
\frac{n_d}{n_{ref}} \sim \epsilon_n^d,\,\frac{|\mathbf{u}|_d}{c_s} \sim \epsilon_u^d,\,\frac{p_d}{p_{ref}} \sim \epsilon_p^d,\,\frac{e \phi_d}{T_{\mathrm{e}0}} \sim \epsilon_\phi^d
```
where ``\epsilon_x`` orders the amplitude of the various fluctuating quantities.  We will assume the same ordering for all fluctuating quantities:
```math
\epsilon_n \sim \epsilon_u \sim \epsilon_p \sim \epsilon_\phi \sim \epsilon.
```
 - Low frequency oscillations of the perturbations:
```math
\frac{1}{\Omega_i}\frac{\partial f_j}{\partial t} \sim \epsilon_\omega \ll 1,\,\,j \geq 1
```
 - Very slow time evolution of background:
```math
\frac{1}{\Omega_i}\frac{\partial f_0}{\partial t} \sim \epsilon_\omega^3
```
 - Anisotropic fluctuations: short, gyroradius scale perpendicular wavelengths and long parallel wavelengths
```math
\rho_\text{s}\nabla_\perp f_j \sim \epsilon^0,\,\,\rho_\text{s}\nabla_\parallel f_j \sim \epsilon ,\,\,j \geq 1
```
 - Long perpendicular background scale lengths, longer parallel background scale lengths:
```math
\rho_\text{s}\nabla_\perp f_0 = \frac{\rho_\text{s}}{L_{ref}} L_{ref}\nabla_\perp f_0 \sim \epsilon,\,\,\rho_\text{s}\nabla_\parallel f_0 = \frac{\rho_\text{s}}{L_{ref}} L_{ref} \nabla_\parallel f_0 \sim \epsilon^2
```
 - Disparate mass ratio:
```math
\frac{m_\mathrm{e}}{m_\mathrm{i}}\sim \epsilon_m \ll 1
```
