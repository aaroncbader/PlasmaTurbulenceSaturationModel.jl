```@meta
CurrentModule = PlasmaTurbulenceSaturationModel
```

# PlasmaTurbulenceSaturationModel

```@index
Pages = ["overview.md"]
```
To use the `PlasmaTurbulenceSaturationModel` (`PTSM`), use the package manager to download and install:
```
julia> using Pkg;
julia> Pkg.add(url="https://gitlab.com/bfaber/PlasmaTurbulenceSaturationModel.jl.git");
julia> using PlasmaTurbulenceSaturationModel
```

## Flux-tube geometry
`PTSM` uses a _flux-tube_ approximation for the magnetic geometry.
The flux-tube approximation simulates a local volume around a central magnetic fieldline and assumes a Taylor expansion in the direction binormal to a magnetic field line:
```math
\mathbf{B}\left(x, y, z\right) = \nabla x(z) \times \nabla y(z) \approx \mathbf{B}_0\left(z\right) + \frac{\partial \mathbf{B}}{\partial x}\Delta x +  \frac{\partial \mathbf{B}}{\partial y}\Delta y + \ldots
```
The normalizations[Normalizations](@ref) are consistent with the gyrokinetic code [GENE](https://genecode.org) and the geometry package GIST.
Thus ``x`` is a radial-like flux coordinate and ``y`` is an angle-like coordinate.
The field line following coordinate ``z`` can be chosen to follow the toroidal or poloidal angle (and implicitly defines ``y`` in a straight field line coordinate system).
For the GENE-like coordinate system, the angle ``z = \theta`` is the poloidal angle and ``y = q \theta - \zeta`` where ``q`` is the safety factor and ``zeta`` is a toroidal-like angle.

Flux tube geometries need to be extracted from equilibrium (re)construction codes like VMEC.
To make use of VMEC equilibria, two optional packages need to be loaded with the `PlasmaTurbulenceSaturationModel`:
  - [`VMEC`](https://gitlab.com/wistell/VMEC.jl)
  - [`GeneTools`](https://gitlab.com/wistell/GeneTools.jl)

## Setting up a PTSM model
A fluid model is represented by the [FluidModel{N,T}](@ref) type, a composite type that holds both the parameters for performing saturation calculations and the spectrum of linear mode calculations.
A fluid model is intialized from an equilibrium geometry and defined by keyword parameters:
```julia
using PlasmaTurbulenceSaturationModel, VMEC, GeneTools

vmec = read_vmec_wout("wout_file.nc")

model = PTSM.setup_linear_model(vmec; # The VMEC flux surface to perform the saturation calculation
                                n_fields=3, # Number of model fields
                                n_ev=1, # Number of eigenvalues 
                                prec=Float64, # Precision for the calculations
                                nkx=11, # Number of kx points including kx = 0
                                nky=10, # Number of ky points
                                Δkx = 0.05, # Spacing between kx points
                                Δky = 0.05, # Spacing between ky points
                                kxSpecLimit = 0.2, # Energy spectrum transition point (kx)
                                kySpecLimit = 0.6, # Energy spectrum transition point (ky)
                                kxSpecPower = 2.0, # Energy spectrum decay power (kx)
                                kySpecPower = 4/3, # Energy spectrum decay power (ky)
                                ∇T = 3.0, # Normalized temperature gradient (-a/T dT/dx)
                                ∇n = 0.0, # Normalized density gradient (-a/n dn/dx)
                                τ = 1.0, # Ratio of electron-to-ion temperature
                                β = 1.0, # Plasma beta
                                solver=ArnoldiMethod(), # Method used solving linear eigenproblem
                                soln_interval=8π, # Length along field line over which to compute linear eigenproblem (radians)
                                ε=200.0, # Hyperdiffusion coefficient for numerical stability
                                errorOrder=4, # Order of the finite difference operators
                                hyperdiffusionOrder=2, # Order for the hyperdiffusion operator
                                quadPoints=1024, # Number of quadrature points for Gauss-Legendre quadrature
                                n_points = 1024, # Number of points over which the linear eigenvalue solution domain is discretized
                            )
```
