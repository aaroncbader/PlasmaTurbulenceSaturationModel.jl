API documentation for `PlasmaTurbulenceSaturationModel.jl`'s internals.

## Model-independent API
### Data types
```@autodocs
Modules = [PlasmaTurbulenceSaturationModel]
Public = false
Pages = ["types.jl",
         "plasma.jl",
         "spectral_grid.jl",
         "linear_problem.jl"
         "drift_wave.jl",
         "fluid_model.jl",
        ]
```

### Geometry
```@autodocs
Modules = [PlasmaTurbulenceSaturationModel]
Public = false
Pages = ["geometry.jl"]
```

### Utilities
```@autodocs
Modules = [PlasmaTurbulenceSaturationModel]
Public = false
Pages = ["utils.jl"]
```

### Linear Solvers
```@autodocs
Modules = [PlasmaTurbulenceSaturationModel]
Public = false
Pages = ["compute_spectrum.jl",
         "linear_solvers.jl",
        ]
```

### Three-wave coupling
```@autodocs
Modules = [PlasmaTurbulenceSaturationModel]
Public = false
Pages = ["coupling.jl]
```

### I/O
```@autodocs
Modules = [PlasmaTurbulenceSaturationModel]
Public = false
Pages = ["input_output.jl"]
```