# PlasmaTurbulenceSaturationModel

[![Dev](https://img.shields.io/badge/docs-dev-blue.svg)](https://bfaber.gitlab.io/PlasmaTurbulenceSaturationModel.jl/dev)
[![Build Status](https://gitlab.com/bfaber/PlasmaTurbulenceSaturationModel.jl/badges/master/pipeline.svg)](https://gitlab.com/bfaber/PlasmaTurbulenceSaturationModel.jl/pipelines)
[![Coverage](https://gitlab.com/bfaber/PlasmaTurbulenceSaturationModel.jl/badges/master/coverage.svg)](https://gitlab.com/bfaber/PlasmaTurbulenceSaturationModel.jl/commits/master)

The PlasmaTurbulenceSaturationModel computes different metrics to evaluate the saturation of drift-wave-driven microturbulence without performing a full nonlinear simulation.  This model is the numerical implementation of the theory described in [Hegna, Terry and Faber, Phys. Plasmas, **25**, 022511 (2018)](https://doi.org/10.1063/1.5018198).

PTSM3D algorithm:

1. Compute eigenvalues $\omega_i$ from a fluid model for drift-wave instabilities (like Ion Temperature Gradient (ITG) modes and Kinetic Ballooning Modes (KBM)) along a flux tube from a stellarator configuration
2. Compute the *three-wave interaction time* 
```math
\tau_{pst}(k,k') = \frac{1}{\mathrm{i}\left(\omega(k'')_t + \omega(k')_s - \omega(k)^\ast_p\right)}
```
and *complex coupling coefficients* 
```math
C(k,k';\omega(k),\omega(k'))
```
from the linear eigenvalue spectra

3. Compute which combination of triplets and wavenumbers maximizes these terms at each *(k_x,k_y)* between unstable and stable modes
